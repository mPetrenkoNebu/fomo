import axios from "axios";

export default class InstructionService {
    // _apiBase = "http://localhost:8080/instruction";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/instruction";

    getInstructions = async () => {
        return await axios.get(`${this._apiBase}/all`)
    }


}