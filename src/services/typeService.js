import axios from "axios";

export default class TypeService {
    // _apiBase = "http://localhost:8080/type";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/type";

    getAllTypes =  async () => {
        return await axios.get(`${this._apiBase}/`)
    }
}