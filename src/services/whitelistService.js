import axios from "axios";

export default class WhitelistService {
    // _apiBase = "http://localhost:8080/whitelist";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/whitelist";

    addWhitelist = async (whitelist, token) => {
        return await axios.post(`${this._apiBase}/add-whitelist`, whitelist,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    getAllWhiteLists =  async () => {
        return await axios.get(`${this._apiBase}/all`)
    }
    getWhitelist = async (id, token) => {
        return await axios.get(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    getWhiteListsByDates = async (startDay, endDay) => {
        return await axios.get(`${this._apiBase}/calendar/${startDay}&${endDay}`)
    }
    deleteWhitelistById = async (id, token) => {
        return await axios.delete(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    editWhitelistById = async (id, whitelist, token) => {
        return await axios.put(`${this._apiBase}/${id}`, whitelist,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }

}