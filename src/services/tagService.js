import axios from "axios";

export default class TagService {
    // _apiBase = "http://localhost:8080/tags";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/tags";

    getAllTags =  async () => {
        return await axios.get(`${this._apiBase}/`)
    }
}