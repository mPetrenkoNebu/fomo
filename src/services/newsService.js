import axios from "axios";

export default class NewsService {
    // _apiBase = "http://localhost:8080/news";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/news";

    addNews = async (news, token) => {
        return await axios.post(`${this._apiBase}/add-news`, news,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    addComment = async (comment, newsId,  token) => {
        return await axios.post(`${this._apiBase}/add-comment`, {comment: comment, newsId: newsId}, {
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    getAllNews =  async () => {
        return await axios.get(`${this._apiBase}/all`)
    }
    getNews = async (id, token) => {
        return await axios.get(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    deleteNewsById = async (id, token) => {
        return await axios.delete(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    editNewsById = async (id, news, token) => {
        return await axios.put(`${this._apiBase}/${id}`, news,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }

}