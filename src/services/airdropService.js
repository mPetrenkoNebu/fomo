import axios from "axios";

export default class AirdropService {
    // _apiBase = "http://localhost:8080/airdrop";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/airdrop";

    addAirdrop = async (airdrop, token) => {
        return await axios.post(`${this._apiBase}/add-airdrop`, airdrop,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    getAllAirdrops =  async () => {
        return await axios.get(`${this._apiBase}/all`)
    }
    getAirdrop = async (id, token) => {
        return await axios.get(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    getAirdropsByType = async (type) => {
        return await axios.get(`${this._apiBase}/type/${type}`)
    }
    getAirdropsByDates = async (startDay, endDay) => {
        return await axios.get(`${this._apiBase}/calendar/${startDay}&${endDay}`)
    }
    deleteAirdropById = async (id, token) => {
        return await axios.delete(`${this._apiBase}/${id}`,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }
    editAirdropById = async (id, airdrop, token) => {
        return await axios.put(`${this._apiBase}/${id}`, airdrop,{
            headers: {
                Authorization: "Bearer " + token,
            },
        })
    }

}