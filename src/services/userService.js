import axios from "axios";

export default class UserService {
    // _apiBase = "http://localhost:8080/user";
    _apiBase = "https://fomo-api-nebu.herokuapp.com/user";

    loginUser = async (token, userName) => {
        return await axios.post(`${this._apiBase}/login`, {userName: userName},{
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }
    checkUser = async (userName) => {
        return await axios.get(`${this._apiBase}/check-user/${userName}`);
    }
    setFilledWhitelist = async (filledId, token, isFilled, isWhitelist) => {
        return await axios.put(`${this._apiBase}/filled`, {filledId: filledId, isFilled: isFilled, isWhitelist: isWhitelist}, {
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }
    setInstructions = async (instructions, token) => {
        return await axios.put(`${this._apiBase}/instruction`, {instructions: instructions}, {
            headers: {
                Authorization: "Bearer " + token,
            },
        });
    }


}