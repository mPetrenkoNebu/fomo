import React, {useEffect, useState} from "react";
import {ChangeButton, NavBar, NavMobile, SlickSlider} from "../../components";
import {Day} from "../../components";
import {romanMonths} from "../../data/romanMonths";
import WhitelistService from "../../services/whitelistService";
import AirdropService from "../../services/airdropService";
import dayjs from "dayjs";
import calendar from 'dayjs/plugin/calendar'


dayjs.extend(calendar)
const Calendar = () => {
    const whitelistService = new WhitelistService();
    const airdropService = new AirdropService();

    const [selectedDay, setSelectedDay] = useState(new Date())
    const [isNavOpen, setIsNavOpen] = useState(false)
    const [selectedCategory, setSelectedCategory] = useState("")
    const [days, setDays] = useState([])
    const [months, setMonths] = useState([])
    const [years, setYears] = useState([])
    const [allDates, setAllDates] = useState([])
    const [whitelists, setWhitelists] = useState([]);
    const [airdrops, setAirdrops] = useState([]);

    const formatDate = (day, month, year) => {
        return dayjs(selectedDay).date(day).month(month).year(year).toDate()
    }

    const handleSearch = () => {
        const dates = []
        document.querySelectorAll(".slick-current").forEach((item) => dates.push(item.innerText.split('\n')[0]));
        dates[1] = romanMonths.indexOf(dates[1]);
        const dateTmp = formatDate(dates[0], dates[1], dates[2])
        setSelectedDay(dateTmp)
    }


    const settings = {
        dots: false,
        infinite: true,
        arrows: false,
        swipeToSlide: true,
        slidesToShow: 3,
        focusOnSelect: true,
        className: "center",
        centerPadding: "35px",
        centerMode: true,
    };

    const handleCategoryChange = (category) => {
        if(category === selectedCategory)
            setSelectedCategory("")
        else setSelectedCategory(category)
    }

    const getLastDayOfMonth = (year, month) => {
        return  new Date(year, month +1, 0).getDate();
    }
    const getDatesOnCalendar = () => {
        let selectedDateTmp = selectedDay;
        let startDay = new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate() - 11))
        const daysArray = [[],[],[]]
        let index = 0;
        for(let i=0; i < 21; i++){
            if(i % 7 === 0 && i !== 0) {
                index++;
            }
            daysArray[index].push(new Date(startDay.setDate(startDay.getDate() + 1)))
        }
        setDays(daysArray)
        startDay = new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate() +11))
    }
    const addWeekOnCalendar = () => {
        const lastDayOnCalendar = days[days.length - 1][6];
        const daysTmp = [[],[]]
        let index = 0
        for(let i=1; i< 15; i++){
            daysTmp[index].push(new Date(lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() + 1)))
                if(i % 7 === 0  && i !== 14){
                index++
            }
        }
        whitelistService.getWhiteListsByDates(  lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() - 14),   lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() + 14).valueOf()).then((response) => {
            setWhitelists(prevWhitelists => [...prevWhitelists, ...response.data.reduce((prev, next) => {
                return prev.concat({
                    ...next,
                    ["type"]: "WHITELIST"
                })
            }, [])])
        })

        airdropService.getAirdropsByDates(lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() - 14),   lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() + 14).valueOf()).then((response) => {
            setAirdrops(prevAirdrops => [...prevAirdrops, ...response.data.reduce((prev, next) => {
                return prev.concat({
                    ...next,
                    ["type"]: "AIRDROP"
                })
            }, [])])
        })
        lastDayOnCalendar.setDate(lastDayOnCalendar.getDate() - 14)
        setDays(prev => [...prev, ...daysTmp])
    }
    const isSmallScreen =  () => {
        return  window.innerWidth > 940
    }
    const fillDatesFilter = () => {
        let startDate = selectedDay.getDate();
        let startMonth = selectedDay.getMonth();
        let startYear = selectedDay.getFullYear();

        const yearsPeriod = 16;
        const lastDate = getLastDayOfMonth(startYear, startMonth)
        const allDatesTmp = [];
        const monthTmp = [];
        const yearsTmp = [];
        for(let i =0; i< lastDate; i++){
            allDatesTmp.push(startDate)
            if(startDate === lastDate)
                startDate = 1;
            else startDate++;
        }
        for (let i =0; i < 12; i++){
            monthTmp.push(romanMonths[startMonth])
            if(startMonth === 11)
                startMonth = 0;
            else startMonth++;
        }
        for(let i =0; i< yearsPeriod; i++){
            yearsTmp.push(startYear)
            if(startYear === 2035)
                startYear = 2019
            startYear++;
        }
        setAllDates(allDatesTmp);
        setMonths(monthTmp);
        setYears(yearsTmp);
    }
    const getDateByDates = () => {

        let selectedDateTmp = selectedDay;
        let startDay = new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate() - 11))
        whitelistService.getWhiteListsByDates(startDay?.valueOf(), new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate() + 21)).valueOf()).then((response) => {
            setWhitelists(response.data.reduce((prev, next) => {
                return prev.concat({
                    ...next,
                    ["type"]: "WHITELIST"
                })
            }, []))
        }).catch(() => {
            setWhitelists([])
        })

        airdropService.getAirdropsByDates(startDay?.valueOf(), new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate())).valueOf()).then((response) => {
            setAirdrops(response.data.reduce((prev, next) => {
                return prev.concat({
                    ...next,
                    ["type"]: "AIRDROP"
                })
            }, []))

        }).catch((err) => {
            setAirdrops([])
        })
        startDay = new Date(selectedDateTmp
            .setDate(selectedDateTmp.getDate() - 10))
    }

    const getFilterResult = (dataEnds, data) => {
        return new Date(dataEnds).getDate() === data.getDate() && new Date(dataEnds).getMonth() === data.getMonth() && new Date(dataEnds).getFullYear() === data.getFullYear()
    }
    useEffect(() => {
        getDateByDates();
        getDatesOnCalendar()
    }, [selectedDay])

    useEffect(() => {
        fillDatesFilter() 
    }, [])
    return (
        <div>
            <NavBar isNavOpen={isNavOpen} setIsNavOpen={setIsNavOpen} />
            <div className={"container-calendar"}>
                <div className={"calendar-wrapper"}>
                    <div className={"calendar-dates"}>
                        <div>
                            <SlickSlider settings={settings}>
                                {allDates?.map((item) => (
                                    <div className={"calendar-data"}>{item}</div>
                                ))}
                            </SlickSlider>
                            <SlickSlider settings={settings}>
                                {months?.map((item) => (
                                    <div className={"calendar-data"}>{item}</div>
                                ))}
                            </SlickSlider>
                            <SlickSlider settings={settings}>
                                {years?.map((item) => (
                                    <div className={"calendar-data--small"}>{item}</div>
                                ))}
                            </SlickSlider>
                            <ChangeButton handleSearch={handleSearch} />
                        </div>

                    </div>
                    <div style={{display: isSmallScreen() ? "block" : "flex", flexWrap:  isSmallScreen() ? 'nowrap': 'wrap', justifyContent: 'center', paddingLeft:  isSmallScreen() ? 0 : '15px'}}>
                        {days?.map((item) => (
                            (isSmallScreen()  ? <div className={"calendar"}>
                                {item?.map((day) => (
                                    <Day key={day.valueOf().toString()} selectedCategory={selectedCategory} selectedDay={selectedDay} day={day} items={[...whitelists.filter((whitelist) => getFilterResult(whitelist.ends, day)), ...airdrops.filter((airdrop) => getFilterResult(airdrop.ends, day))]} />
                                ))}
                            </div> :
                                    (item?.map((day) => (
                                    <Day key={day.valueOf().toString()} selectedCategory={selectedCategory} selectedDay={selectedDay} day={day} items={[...whitelists.filter((whitelist) => getFilterResult(whitelist.ends, day)), ...airdrops.filter((airdrop) => getFilterResult(airdrop.ends, day))]} />
                                )))
                            )
                        ))}
                    </div>
                    <div onClick={() => addWeekOnCalendar()}  className={"calendar-arrow"}>
                        <img src={require('../../assets/icons/arrow-down-icon.svg').default} alt=""/>
                    </div>
                    <div className={"calendar-buttons-wrapper"}>
                        <div className={"calendar-buttons"}>
                            <div onClick={() => handleCategoryChange("WHITELISTS")} style={{color: `${selectedCategory !== "WHITELISTS" && selectedCategory !== "" ? "#555656" : "#00FFD8"}`, border: `1px solid ${selectedCategory !== "WHITELISTS" && selectedCategory !== "" ? "#555656" : "#00FFD8"}`}} className={`calendar-button`}>WHITELISTS</div>
                            <div onClick={() => handleCategoryChange("AIRDROPS")} style={{color: `${selectedCategory !== "AIRDROPS" && selectedCategory !== "" ? "#555656" : "#FFEB00"}`, border: `1px solid ${selectedCategory !== "AIRDROPS" && selectedCategory !== "" ? "#555656" : "#FFEB00"}`}} className={`calendar-button`}>AIRDROPS</div>
                            <div onClick={() => handleCategoryChange("EVENTS")} style={{color: `${selectedCategory !== "EVENTS" && selectedCategory !== "" ? "#555656" : "#9C00D6"}`, border: `1px solid ${selectedCategory !== "EVENTS" && selectedCategory !== "" ? "#555656" :"#9C00D6"}`}} className={`calendar-button`}>EVENTS</div>
                        </div>
                        <div className={"calendar-warning"}>
                            <img  className={"calendar-warning-icon"} src={require('../../assets/icons/warning.svg').default} alt=""/>
                            <div  className={"calendar-warning-text"}>- not filled</div>
                        </div>
                    </div>

                </div>

            </div>
            <NavMobile isNavOpen={isNavOpen} />
        </div>
    )
}

export default Calendar;