import React from "react";
import {AddAction, DeleteAction} from "../../../components";
import EditAction from "../../../components/EditAction";

const Actions = () => {
    return (
        <div className={"admin-action"}>
            <div className={"admin-action-wrapper"}>
                <div style={{ width: '40px', height: '40px'}} className={"admin-action-icon-wrapper"}>
                    <img className={"admin-action-icon"} src={require('../../../assets/icons/plus-icon.svg').default} alt=""/>
                </div>
                <div>
                    <AddAction action={"ADD WHITELIST"} link={"/admin/add-whitelist"} />
                    <AddAction action={"ADD AIRDROP"} link={"/admin/add-airdrops"} />
                    <AddAction action={"ADD NEWS"} link={"/admin/add-news"} />
                    <AddAction action={"ADD RESULTS"} link={"/admin/add-results"} />
                </div>
            </div>

            <div  className={"admin-action-wrapper"}>
                <div style={{ width: '40px', height: '40px'}} className={"admin-action-icon-wrapper"}>
                    <img className={"admin-action-icon"} src={require('../../../assets/icons/minus-icon.svg').default} alt=""/>
                </div>
                <div>
                    <DeleteAction action={"DELETE WHITELIST"} link={"/admin/delete-whitelist"} />
                    <DeleteAction action={"DELETE AIRDROP"} link={"/admin/delete-airdrop"}/>
                    <DeleteAction action={"DELETE NEWS"} link={"/admin/delete-news"}/>
                </div>
            </div>
            <div  className={"admin-action-wrapper"}>
                <div style={{padding: '18px 0'}} className={"admin-action-icon-wrapper"}>
                    <img style={{width: '15px', height: '20px'}} className={"admin-action-icon"} src={require('../../../assets/icons/edit-icon.svg').default} alt=""/>
                </div>
                <div>
                    <EditAction action={"EDIT WHITELIST"}  link={"/admin/edit-whitelist"}/>
                    <EditAction action={"EDIT AIRDROP"} link={"/admin/edit-airdrop"}/>
                    <EditAction action={"EDIT NEWS"} link={"/admin/edit-news"} />
                </div>
            </div>
        </div>
    )
}

export default Actions;