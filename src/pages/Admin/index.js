import React, {useState} from "react";
import {NavBarAdmin, NavMobile} from "../../components";
import Actions from "./Actions";
import Logo from "../../components/Logo";

const Admin = () => {
    const [isNavOpen, setIsNavOpen] = useState(false)
    return (
        <div>
            <NavBarAdmin setIsNavOpen={setIsNavOpen} isNavOpen={isNavOpen} isBack={true}/>
            <div className={"container"}>
                <Actions />
            </div>
            <NavMobile isNavOpen={isNavOpen} />
        </div>
    )
}

export default Admin;