import React, {useEffect, useState} from "react";
import {DeleteElement, NavBarAdmin} from "../../components";
import NewsService from "../../services/newsService";
import {useAuth} from "../../contexts/AuthProvider";

const DeleteNews = () => {
    const newsService = new NewsService();

    const { userToken } = useAuth();

    const [news, setNews] = useState([]);

    const handleDeleteNews = (id) => {
        setNews(prev => prev.filter((news) => news.id !== id))
        newsService.deleteNewsById(id, userToken).then(() => {

        });
    }

    useEffect(() => {
        newsService.getAllNews().then((response) => {
            setNews(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {news?.map((airdrop) => (
                    <DeleteElement name={airdrop?.title} data={airdrop?.data} id={airdrop?.id} handleDelete={handleDeleteNews} />
                ))}
            </div>
        </div>
    )
}

export default DeleteNews;