import React, {useEffect, useState} from "react";
import {
    Breadcrumbs,
    Description,
    Filled,
    Info,
    Instruction,
    NavBar,
    TabsButton,
    Title
} from "../../components";
import AirdropService from "../../services/airdropService";
import {useAuth} from "../../contexts/AuthProvider";

const Airdrop = ({match}) => {
    const airdropService = new AirdropService();
    const {userToken} = useAuth();
    const [airdrop, setAirdrop] = useState(null)

    const handleAirdropEnd = () => {
        const data = new Date(airdrop?.ends);
        const dataNow = new Date();
        if(dataNow?.valueOf() > data?.valueOf())
            return "0 days"
        let delta = Math.abs(data - dataNow) / 1000;
        const days = Math.floor(delta / 86400);
        delta -= days * 86400;
        const hours = Math.floor(delta / 3600) % 24;
        return  `${days} days ${hours} hours`
    }
    const openInSeparateTabs =() => {
        airdrop?.instructions?.forEach((instruction) => {
            window.open(instruction.value,'_blank' )
        })
    }

    useEffect(() => {
        airdropService.getAirdrop(match.params.airdropId, userToken).then((response) => {
            setAirdrop(response.data)
        })
    }, [userToken])
    return (
        <div>
            <NavBar />
            <div className={"container"}>
                <Breadcrumbs pathItems={[{link: "/airdrops", name: "Airdrops"},{link: "", name: "Kylin"}]}/>

            </div>
            <div className={"container-main"}>
                <div className={"wrapper"}>
                        <Title icon={airdrop?.iconUrl} name={"Kylin.network"}/>
                        <Filled isFilled={airdrop?.isFilled} id={match.params.airdropId} isWhitelist={false} />
                    <div className={"wrapper"}>
                        <Info items={[{key: "Reward", value: airdrop?.reward},{key: "Airdrop ends", value: handleAirdropEnd()},{key: "Link", value: airdrop?.website}]} />
                        <Instruction instructions={airdrop?.instructions} />
                        <TabsButton openInSeparateTabs={openInSeparateTabs} />
                        <Description description={airdrop?.description} />
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Airdrop;