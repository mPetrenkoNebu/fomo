import React, {useEffect, useState} from "react";
import {
    ActionButton,
    ActionTitle,
    AddButton,
    Input,
    InputDate,
    InstructionSelect,
    NavBarAdmin,
    Preloader
} from "../../components";
import AirdropService from "../../services/airdropService";
import InstructionService from "../../services/instructionService";
import {useAuth} from "../../contexts/AuthProvider";

const EditAirdrop = ({match}) => {
    const airdropService = new AirdropService()
    const instructionService = new InstructionService();

    const { userToken } = useAuth();

    const [isDisable, setIsDisable] = useState(false);
    const [fields, setFields] = useState([]);
    const [instructions, setInstructions] = useState([]);
    const [selectInstructions, setSelectInstructions] = useState([
        {id: 0, name: "", value: ""}
    ]);
    const [airdrop, setAirdrop] = useState({
        isFilled: false,
        iconUrl: "",
        project: "",
        instructions: [],
        reward: "",
        types: "",
        ends: "",
        starts: "",
        description: "",
        status: "",
        creatorId: "123",
        creatorName:"Test"
    })
    const handleAddField = () => {
        setFields(prev => [...prev, instructions])
    }
    const handleAirdropChange = (name, value) => {
        setAirdrop((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleEditAirdrop = () => {
        setIsDisable(true)
        airdropService.editAirdropById(match.params?.airdropId, airdrop, userToken).then(() => {
            setIsDisable(false)
        })

    }
    const handleAddInstruction = (index, instruction, value) => {
        if(instruction !== "" && instruction !== undefined)
            setSelectInstructions(prev => prev.map((item) => item.id === index ? {
                id: index, name: instruction, value: value
            } : item))
    }
    useEffect(() => {
        airdropService.getAirdrop(match.params?.airdropId, userToken).then((response) => {
            setAirdrop(response.data)
            setSelectInstructions(response.data.instructions)
            return response.data
        }).then((result) => {
            instructionService.getInstructions().then((response) => {
                setInstructions(response.data)
                setFields(result?.instructions?.length > 1 ? result?.instructions?.map((item) => {
                    return response.data
                }) : [response.data])
            })
        })


    }, [userToken])
    useEffect(() => {
        if( fields.length !== 0 && airdrop?.instructions.length !== fields.length){
            setSelectInstructions(prev => [...prev, {id: prev.length, name: "", value: ""}])
        }
    },  [fields])
    useEffect(() => {
            handleAirdropChange("instructions", selectInstructions)
    }, [selectInstructions])
    const getDate = (date) => {
        const tmpDate = new Date(date);
        return `${tmpDate.getFullYear()}-${tmpDate.getMonth() < 10 ? `0${tmpDate.getMonth()}` : tmpDate.getMonth()}-${tmpDate.getDate() < 10 ? `0${tmpDate.getDate() }` : tmpDate.getDate() }`
    }
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"EDIT AIRDROP"} actionType={"EDIT"} />
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: '75px'}}>
                        <Input handleInputChange={handleAirdropChange} name={"project"} label={"project"} value={airdrop?.project} />
                        <Input handleInputChange={handleAirdropChange} name={"website"} label={"website"} value={airdrop?.website} />
                    </div>
                    <div>
                        <Input handleInputChange={handleAirdropChange} name={"reward"}  label={"reward"} value={airdrop?.reward}/>
                        <Input handleInputChange={handleAirdropChange} name={"type"} label={"type"} value={airdrop?.type}/>
                        <InputDate handleInputChange={handleAirdropChange}  name={"starts"}  label={"starts"}  value={getDate(airdrop?.starts)} />
                        <InputDate handleInputChange={handleAirdropChange}  name={"ends"}  label={"ends"} value={getDate(airdrop?.ends)} />
                    </div>
                </div>
                <div style={{marginBottom: '90px', position: 'relative'}}>
                    <div style={{marginBottom: "20px"}} className={"title"}>Instruction</div>
                    <div>
                        <div>
                            {fields.map((item, index) => (
                                <InstructionSelect  onChange={handleAddInstruction} items={item} index={index} instruction={airdrop?.instructions[index]}/>
                            ) )}
                        </div>
                    </div>
                    <AddButton handleAddField={handleAddField} />
                </div>
                <ActionButton onClick={handleEditAirdrop} isDisable={isDisable} name={"EDIT"} />
            </div>
            {isDisable ? <Preloader /> : ""}

        </div>
    )
}

export default EditAirdrop;