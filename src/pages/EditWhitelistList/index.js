import React, {useEffect, useState} from "react";
import { EditElement, NavBarAdmin} from "../../components";
import WhitelistService from "../../services/whitelistService";

const EditWhitelistList = () => {
    const whitelistService = new WhitelistService();

    const [whitelists, setWhitelists] = useState([]);


    useEffect(() => {
        whitelistService.getAllWhiteLists().then((response) => {
            setWhitelists(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {whitelists?.map((whitelist) => (
                    <EditElement name={whitelist?.name} data={whitelist?.ends} link={`edit-whitelist/${whitelist?.id}`}  />
                ))}
            </div>
        </div>
    )
}

export default EditWhitelistList;