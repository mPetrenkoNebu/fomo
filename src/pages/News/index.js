import React, { useEffect, useState } from "react";
import {NavBar, NavMobile} from "../../components";
import Information from "./Information";
import Comments from "./Comments";
import Tags from "./Tags";
import NewsService from "../../services/newsService";
import MetaTags from "react-meta-tags";
import {useAuth} from "../../contexts/AuthProvider";

const News = ({ match }) => {
    const { currentUser, userToken} = useAuth();
  const newsService = new NewsService();
    const [isNavOpen, setIsNavOpen] = useState(false)


  const [news, setNews] = useState(null);
  const [comments, setComments] = useState([])
    const handleAddComment = (comment) => {
      const commentData = {comment: comment, userName: currentUser.name, data: new Date()}
      setComments(prev => [...prev, commentData])
        newsService.addComment(commentData,match.params?.newsId,  userToken)
    }

  useEffect(() => {
    newsService.getNews(match.params?.newsId, userToken).then((response) => {
      setNews(response.data);
      setComments(response.data.comments)
    });
  }, [userToken]);
  return (
    <div>
      <MetaTags>
        <meta name="description" content={news?.text?.split(".")[0]} />
        <title>{news?.title}</title>
      </MetaTags>
        <NavBar isNavOpen={isNavOpen} setIsNavOpen={setIsNavOpen} />
      <div className={"container"}>
        <Information news={news} />
        <Tags tags={news?.tags?.concat(news?.customTag)} />
        <Comments quantity={comments?.length} comments={comments} handleAddComment={handleAddComment} />
      </div>
        <NavMobile isNavOpen={isNavOpen} />
    </div>
  );
};

export default News;
