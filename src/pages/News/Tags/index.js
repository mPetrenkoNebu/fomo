import React from "react";
import {Tag} from "../../../components";

const Tags = ({tags}) => {
    return (
        <div className={"information-tags"}>
            {tags?.map((tag, index) => (
                <Tag key={tag?.tag ? tag.tag.toString + index : tag.toString() + index}  name={tag}/>

            ))}
        </div>
    )
}

export default Tags;