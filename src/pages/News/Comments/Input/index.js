import React, {useState} from "react";

const Input = ({handleAddComment}) => {
    const [comment, setComment] = useState("")
    const addComment = () => {
        handleAddComment(comment);
        setComment("")
    }
    return (
        <div className={"comments-input--wrapper"}>
            <textarea onChange={(e) => setComment(e.target.value)} className={"comments-input"} placeholder={"Type your comment"}  />
            <div onClick={() => addComment()} className={"comments-button"}>
                add
            </div>
        </div>
    )
}

export default Input;