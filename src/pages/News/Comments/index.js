import React from "react";
import { Comment } from "../../../components";
import Input from "./Input";

const Comments = ({ quantity, comments, handleAddComment }) => {
  return (
    <div className={"comments"}>
      <div className={"comments-title"}>Comments({quantity})</div>
      <div>
          {comments?.map((comment) => (
              <Comment
                  author={comment?.userName}
                  comment={comment?.comment }
                  data={comment?.data}
              />
          ))}
      </div>
        <Input handleAddComment={handleAddComment} />
    </div>
  );
};

export default Comments;
