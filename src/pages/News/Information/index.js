import React from "react";
import {romanMonths} from "../../../data/romanMonths";

const Information = ({news}) => {
    const getDate = () => {
        if(!news?.data)
            return ""
        const tmpDate = new Date(news?.data);
        return `${tmpDate.getDate()}.${romanMonths[tmpDate.getMonth()]}.${tmpDate.getFullYear()}`
    }
    return (
        <div className={"information"}>
            <div className={"news--wrapper"}>
                <div className={"news-image--wrapper"}>
                    <img className={"news-image"} src={news?.photo} alt="" />
                </div>
                <div className={"news-info information-title"}>
                    <div className={"news-title--big"}>{news?.title}</div>
                </div>
                <div className={"news-data"}>{getDate()}</div>
            </div>
            <div style={{marginTop: '30px'}}>
                {news?.text?.split("*!brk!*").map((item) => (
                        <div className={"information-text"}>{item}</div>
                ))}
            </div>

        </div>
    )
}

export default Information;