import React, {useState} from "react";
import {NavBar,  NavMobile} from "../../components";
import News from "./News";

const Main = () => {
    const [isNavOpen, setIsNavOpen] = useState(false)
    return (
        <div>
            <NavBar isNavOpen={isNavOpen} setIsNavOpen={setIsNavOpen} />
            <div className={"container"}>
                <News />
            </div>
            <NavMobile isNavOpen={isNavOpen} />
        </div>
    )
}

export default Main;