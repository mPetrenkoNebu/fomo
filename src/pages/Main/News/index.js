import React, {useEffect, useState} from "react";
import { NewsItem } from "../../../components";
import NewsService from "../../../services/newsService";

const News = () => {
    const newsService = new NewsService();

    const [news, setNews] = useState([]);

    useEffect(() => {
        newsService.getAllNews().then((response) => {
            setNews(response.data)
        })
    },[])

  return (
    <div className={"news"}>
        {news?.map((item) => (
            <NewsItem
                title={item?.title}
                text={item?.text}
                imageUrl={item?.photo}
                data={item?.data}
                id={item?.id}
            />
        ))}
    </div>
  );
};

export default News;
