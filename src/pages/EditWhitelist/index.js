import React, {useEffect, useState} from "react";
import WhitelistService from "../../services/whitelistService";
import InstructionService from "../../services/instructionService";
import {
    ActionButton,
    ActionTitle,
    AddButton,
    ImagePicker,
    Input,
    InputDate,
    InstructionSelect,
    NavBarAdmin, Preloader
} from "../../components";
import {useAuth} from "../../contexts/AuthProvider";

const EditWhitelist = ({match}) => {
    const whitelistService = new WhitelistService();
    const instructionService = new InstructionService();

    const { userToken } = useAuth();

    const [isDisable, setIsDisable] = useState(false);
    const [fields, setFields] = useState([]);
    const [instructions, setInstructions] = useState([]);
    const [selectInstructions, setSelectInstructions] = useState([
        {id: 0, name: "", value: ""}
    ]);
    const [whitelist, setWhitelist] = useState({
        isFilled: false,
        iconUrl: "",
        name: "",
        website: "",
        platform: "",
        hardCap: "",
        ends: "",
        description: "",
        instructions: [],
        creatorId: "SZCkGSblgRP01o8PCYJPq5dzk3E3",
        creatorName: "TestUser"
    })
    const handleAddField = () => {
        setFields(prev => [...prev, instructions])
    }
    const handleChangeImage = (e) => {
        const file = e.target.files[0]
    }
    const handleWhitelistChange = (name, value) => {
        setWhitelist((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleEditWhitelist = () => {
        setIsDisable(true)
        whitelistService.editWhitelistById(match.params.whitelistId, whitelist, userToken).then(() => {
            setIsDisable(false)
        })

    }
    const handleAddInstruction = (index, instruction, value) => {
        if(instruction !== "" && instruction !== undefined)
            setSelectInstructions(prev => prev.map((item) => item.id === index ? {
                id: index, name: instruction, value: value
            } : item))
    }
    useEffect(() => {

        whitelistService.getWhitelist(match.params.whitelistId, userToken).then((response) => {
            setWhitelist(response.data)
            setSelectInstructions(response.data.instructions)
            return response.data
        }).then((result) => {
            instructionService.getInstructions().then((response) => {
                setInstructions(response.data)
                setFields(result?.instructions?.length > 1 ? result?.instructions?.map((item) => {
                    return response.data
                }) : [response.data])
            })
        })
    }, [userToken])
    useEffect(() => {
        if( fields.length !== 0 && whitelist?.instructions.length !== fields.length){
            setSelectInstructions(prev => [...prev, {id: prev.length, name: "", value: ""}])
        }
    },  [fields])
    useEffect(() => {
            handleWhitelistChange("instructions", selectInstructions)
    }, [selectInstructions])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"EDIT WHITELIST"} actionType={"EDIT"} />
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: '75px'}}>
                        <Input handleInputChange={handleWhitelistChange} name={"project"}   label={"project"} value={whitelist?.project} />
                        <Input handleInputChange={handleWhitelistChange} name={"website"} label={"website"} value={whitelist?.website} />
                        <Input handleInputChange={handleWhitelistChange} name={"platform"} label={"platform"} value={whitelist?.platform} />
                        <Input handleInputChange={handleWhitelistChange} name={"hardCap"} label={"hard cap"} value={whitelist?.hardCap}/>
                        <Input handleInputChange={handleWhitelistChange} name={"name"} label={"name"} value={whitelist?.name} />
                    </div>
                    <div>
                        <Input handleInputChange={() => {}} name={"personal cap"}  label={"personal cap"} />
                        <Input handleInputChange={handleWhitelistChange} name={"whitelist"} label={"whitelist"} value={whitelist?.whitelist} />
                        <InputDate handleInputChange={handleWhitelistChange}  name={"starts"}  label={"starts"} />
                        <InputDate handleInputChange={handleWhitelistChange}  name={"ends"}   label={"ends"}/>
                        <ImagePicker onChange={handleChangeImage} name={"Logo"} />
                    </div>
                </div>
                <div style={{marginBottom: '90px', position: 'relative'}}>
                    <div style={{marginBottom: "20px"}} className={"title"}>Instruction</div>
                    <div>
                        <div>
                            {fields?.map((item, index) => (
                                <InstructionSelect key={index} onChange={handleAddInstruction} items={item} index={index} instruction={whitelist?.instructions[index]}/>
                            ) )}
                        </div>
                    </div>
                    <AddButton handleAddField={handleAddField} />
                </div>
                <ActionButton onClick={handleEditWhitelist} isDisable={isDisable} name={"EDIT"} />
            </div>
            {isDisable ? <Preloader /> : ""}
        </div>
    )
}

export default EditWhitelist;