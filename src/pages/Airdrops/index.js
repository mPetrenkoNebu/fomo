import React, {useState} from "react";
import {NavBar, NavMobile} from "../../components";
import Table from "./Table";

const Airdrops = () => {
    const [isNavOpen, setIsNavOpen] = useState(false)

    return (
        <div>
            <NavBar isNavOpen={isNavOpen} setIsNavOpen={setIsNavOpen} />
            <div className={"container"}>
                <Table />
            </div>
            <NavMobile isNavOpen={isNavOpen} />
        </div>
    )
}

export default Airdrops;