import React, {useEffect, useState} from "react";
import {
    TableData,
    TableFilled,
    TableHyperLink,
    TableIcon,
    TableReward, TableStatus,
    TableText,
    TableType,
    THead, TypeButton
} from "../../../components";
import AirdropService from "../../../services/airdropService";
import TypeService from "../../../services/typeService";
import {useAuth} from "../../../contexts/AuthProvider";

const Table = () => {
    const airdropService = new AirdropService();
    const typeService = new TypeService();

    const { currentUser } = useAuth();

    const [airdrops, setAirdrops] = useState([]);
    const [types, setTypes] = useState([])

    const [selectedType, setSelectedType] = useState(null)

    const handleChangeSelectedType = (type) => {
        setSelectedType(prev => prev?.name === type?.name  ? null : type)
    }

    useEffect(() => {
        airdropService.getAllAirdrops().then((response) => {
            setAirdrops(response.data)
        })
        typeService.getAllTypes().then((response) => {
            setTypes(response.data)
        })
    }, [])
    useEffect(() => {
        if(selectedType){
            airdropService.getAirdropsByType(selectedType.name).then((result) => {
                setAirdrops(result.data)
            }).catch(() => {
                setAirdrops([])
            })
        }else{
            airdropService.getAllAirdrops().then((response) => {
                setAirdrops(response.data)
            })
        }
    }, [selectedType])

    return (
        <div className={"table--wrapper"}>
            <div className={"type-button-wrapper"}>
                <div className={"type-button-title"}>Filter</div>
                {types?.map((item) => (
                    <TypeButton type={item} onClick={handleChangeSelectedType} selectedType={selectedType}/>
                ))}
            </div>
            <table className={"table"}>
                <THead items={["filled?", "", "project", "reward", "type", "end", "link", "status"]} />
                <tbody>
                {airdrops.map((item) => (
                    <tr className={"table-tr"} key={item.id}>
                        <td><TableFilled isFilled={currentUser?.filledAirdrops?.includes(item.id)} /> </td>
                        <td><TableIcon icon={item.iconUrl} /> </td>
                        <td><TableText text={item.project} /> </td>
                        <td><TableReward reward={item.reward} /> </td>
                        <td><TableType types={item.types}/></td>
                        <td><TableData data={item.ends}/></td>
                        <td><TableHyperLink link={`/airdrops/${item.id}`} /> </td>
                        <td><TableStatus isActive={!(new Date().valueOf() > new Date(item?.ends))} /> </td>
                    </tr>
                ))}
                </tbody>
            </table>

        </div>
    )
}

export default Table;