import React, {useEffect, useState} from "react";
import {Description, Filled, Info, Instruction, NavBar, TabsButton} from "../../components";
import {Breadcrumbs, Title} from "../../components";
import WhitelistService from "../../services/whitelistService";
import {useAuth} from "../../contexts/AuthProvider";

const Whitelist = ({match}) => {
    const whitelistService = new WhitelistService();

    const { userToken } = useAuth();

    const [whitelist, setWhitelist] = useState(null)

    const handleAirdropEnd = () => {
        const data = new Date(whitelist?.ends);
        const dataNow = new Date();
        if (dataNow.valueOf() > data.valueOf())
            return "0 days"
        let delta = Math.abs(data - dataNow) / 1000;
        const days = Math.floor(delta / 86400);
        delta -= days * 86400;
        const hours = Math.floor(delta / 3600) % 24;

    }
        const openInSeparateTabs =() => {
        whitelist?.instructions?.forEach((instruction) => {
            window.open(instruction.value,'_blank' )
        })
    }

    useEffect(() => {
        if(match.params.whitelistId){
            whitelistService.getWhitelist(match.params.whitelistId, userToken).then((response) => {
                setWhitelist(response.data)
            })
        }

    },[userToken])
    return (
        <div>
            <NavBar />
            <div className={"container"}>
                <Breadcrumbs pathItems={[{link: "/whitelists", name: "Whitelists"},{link: "", name: "Kylin"}]}/>

            </div>
            <div className={"container-main"}>
                <div className={"wrapper"}>

                        <Title icon={whitelist?.iconUrl} name={whitelist?.name}/>
                        <Filled isFilled={whitelist?.isFilled} id={match.params.whitelistId} isWhitelist={true}/>
                    <div className={"wrapper-item"}>
                        <Info items={[{key: "Personal Cap", value: `$${whitelist?.hardCap}`},{key: "Whitelists closes", value: handleAirdropEnd()},{key: "Platform", value: whitelist?.platform},{key: "Website", value: whitelist?.website},{key: "Vesting period", value: whitelist?.vestingPeriod}]} />
                        <Instruction instructions={whitelist?.instructions} />
                        <TabsButton openInSeparateTabs={openInSeparateTabs}/>
                        <Description description={whitelist?.description} />
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Whitelist;