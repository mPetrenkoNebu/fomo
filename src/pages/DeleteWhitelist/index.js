import React, {useEffect, useState} from "react";
import {DeleteElement, NavBarAdmin} from "../../components";
import WhitelistService from "../../services/whitelistService";
import {useAuth} from "../../contexts/AuthProvider";

const DeleteWhitelist = () => {
    const whiteListService = new WhitelistService();

    const { userToken } = useAuth();

    const [whitelists, setWhitelists] = useState([]);

    const handleDeleteWhitelist = (id) => {
        setWhitelists(prev => prev.filter((whitelist) => whitelist.id !== id))
        whiteListService.deleteWhitelistById(id, userToken).then(() => {

        });
    }

    useEffect(() => {
        whiteListService.getAllWhiteLists().then((response) => {
            setWhitelists(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {whitelists?.map((whitelist) => (
                    <DeleteElement name={whitelist?.name} data={whitelist?.ends} id={whitelist?.id} handleDelete={handleDeleteWhitelist} />
                ))}
            </div>
        </div>
    )
}

export default DeleteWhitelist;