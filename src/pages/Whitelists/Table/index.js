import React, {useEffect, useState} from "react";
import {TableData, TableFilled, TableHyperLink, TableIcon, TableInfo, TableText, THead} from "../../../components";
import WhitelistService from "../../../services/whitelistService";
import {useAuth} from "../../../contexts/AuthProvider";

const Table = () => {
    const whitelistService = new WhitelistService();

    const { currentUser } = useAuth();

    const [whitelists, setWhitelists] = useState([])


    useEffect(() => {
        whitelistService.getAllWhiteLists().then((response) => {
            setWhitelists(response.data)
        })
    }, [])
    return (
        <div className={"table--wrapper"}>
            <table className={"table"}>
                <THead items={["filled?", "", "name", "website", "platform", "hard cap", "ends", "whitelist", "instructions"]}/>
                <tbody>
                {whitelists?.map((item) => (
                    <tr className={"table-tr"} key={item.id}>
                        <td><TableFilled isFilled={currentUser?.filledWhitelists?.includes(item.id)}/></td>
                        <td><TableIcon icon={item.iconUrl}/></td>
                        <td><TableText text={item.name}/></td>
                        <td><TableHyperLink link={item.website} /> </td>
                        <td><TableText text={item.platform} /> </td>
                        <td><TableText text={`$${item.hardCap}`} /> </td>
                        <td><TableData isGood={!(new Date().valueOf() > new Date(item?.ends))} data={item.ends} /></td>
                        <td><TableHyperLink link={`/whitelists/${item.id}`} /> </td>
                        <td><TableInfo link={`/whitelists/${item.id}`} /> </td>

                        {item.isResult ? <div  className={"table-button"}>
                            results
                        </div> : <></>}

                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    )
}

export default Table;