import React, { useEffect, useState } from "react";
import {
  ActionButton,
  ActionTitle,
  AddButton, AuthenticationErrors, CloseIcon,
  ImagePicker,
  Input,
  InputDate,
  InstructionSelect,
  NavBarAdmin,
  Preloader, TextArea,
} from "../../components";
import WhitelistService from "../../services/whitelistService";
import InstructionService from "../../services/instructionService";
import app from "../../firebase";
import {Toast} from "../../components/Toast/toast";
import {useHistory} from 'react-router-dom'
import {useAuth} from "../../contexts/AuthProvider";


const AddWhitelist = () => {
  const { currentUser, userToken } = useAuth();

  const whitelistService = new WhitelistService();
  const instructionService = new InstructionService();

  const history = useHistory()

  const [errors, setErrors] = useState("");
  const [isDisable, setIsDisable] = useState(false);
  const [fields, setFields] = useState([]);
  const [image, setImage] = useState(null);
  const [file, setFile] = useState(null);
  const [instructions, setInstructions] = useState([]);
  const [selectInstructions, setSelectInstructions] = useState([
    { id: 0, name: "", value: "" },
  ]);
  const [whitelist, setWhitelist] = useState({
    isFilled: false,
    iconUrl: "",
    name: "",
    website: "",
    platform: "",
    hardCap: "",
    ends: "",
    description: "",
    vestingPeriod: "",
    instructions: [],
    creatorId: currentUser.uid,
    creatorName: currentUser.name
  });
  const handleAddField = () => {
    setFields((prev) => [...prev, instructions]);
  };
  const handleChangeImage = (e) => {
    const file = e.target.files[0];
    if (file.type.startsWith("image")) {
      setFile(URL.createObjectURL(file));
      setImage(file);
    }
  };
  const handleWhitelistChange = (name, value) => {
    setWhitelist((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };
  const handleUploadImage = async () => {
    const storageRef = app.storage().ref();
    const fileRef = storageRef.child(
      `whitelists/${new Date().valueOf()}-${image.name}`
    );
    await fileRef.put(image);
    return await fileRef.getDownloadURL();
  };
  const isFieldsAreNotFilled = () => {
    return whitelist.project === ""
        || whitelist.description === ""
        || whitelist.ends === ""
        || whitelist.reward === ""
        || whitelist.platform === ""
        || whitelist.hardCap === ""
        || whitelist.name === ""
        || whitelist.vestingPeriod === ""
        || whitelist.description === ""
        || !image
  }
  const handleAddWhitelist = async () => {
    setIsDisable(true);
    setErrors("")
    if(isFieldsAreNotFilled()){
      setErrors("All fields must be filled!")
      setIsDisable(false)
    }else{
      const imageUrl = await handleUploadImage();
      const whitelistTmp = { ...whitelist, ["iconUrl"]: imageUrl };
      whitelistService.addWhitelist(whitelistTmp, userToken).then(() => {
        setIsDisable(false);
        history.push('/admin')
        Toast.add({
          text: "Whitelist is correctly added!",
          color: "#6eaf0f",
          autohide: true,
          delay: 3000,
        });
      });
    }
  };
  const handleAddInstruction = (index, instruction, value) => {
    setSelectInstructions((prev) =>
      prev.map((item) =>
        item.id === index
          ? {
              id: index,
              name: instruction,
              value: value,
            }
          : item
      )
    );
  };
  useEffect(() => {
    instructionService.getInstructions().then((response) => {
      setInstructions(response.data);
      setFields([response.data]);
    });
  }, []);
  useEffect(() => {
    if (fields.length !== 1 && fields.length !== 0) {
      setSelectInstructions((prev) => [
        ...prev,
        { id: prev.length, name: "", value: "" },
      ]);
    }
  }, [fields]);
  useEffect(() => {
    handleWhitelistChange("instructions", selectInstructions);
  }, [selectInstructions]);
  return (
    <div>
      <NavBarAdmin isBack={false} />
      <div className={"container"}>
        <ActionTitle name={"ADD WHITELIST"} actionType={"ADD"} />
        {errors !== "" ? <AuthenticationErrors  errors={errors}/> : ""}
        <div style={{ display: "flex" }}>
          <div style={{ marginRight: "75px" }}>
            <Input
              handleInputChange={handleWhitelistChange}
              name={"project"}
              label={"project"}
            />
            <Input
              handleInputChange={handleWhitelistChange}
              name={"website"}
              label={"website"}
            />
            <Input
              handleInputChange={handleWhitelistChange}
              name={"platform"}
              label={"platform"}
            />
            <Input
              handleInputChange={handleWhitelistChange}
              name={"hardCap"}
              label={"hard cap"}
            />
            <Input
              handleInputChange={handleWhitelistChange}
              name={"name"}
              label={"name"}
            />
            <Input
                handleInputChange={handleWhitelistChange}
                name={"vestingPeriod"}
                label={"Vesting Period"}
            />
          </div>
          <div>
            <Input
              handleInputChange={() => {}}
              name={"personal cap"}
              label={"personal cap"}
            />
            <Input
              handleInputChange={handleWhitelistChange}
              name={"whitelist"}
              label={"whitelist"}
            />
            <InputDate
              handleInputChange={handleWhitelistChange}
              name={"starts"}
              label={"starts"}
            />
            <InputDate
              handleInputChange={handleWhitelistChange}
              name={"ends"}
              label={"ends"}
            />
            <ImagePicker onChange={handleChangeImage} name={"Logo"} />
          </div>
        </div>
        {file ? <img style={{ maxHeight: "500px" }} src={file} alt="" /> : ""}
        <div style={{ marginBottom: "90px", position: "relative" }}>
          <div style={{ marginBottom: "20px" }} className={"title"}>
            Instruction
          </div>
          <div>
            <div>
              {fields.map((item, index) => (
                <InstructionSelect
                  onChange={handleAddInstruction}
                  items={item}
                  index={index}
                />
              ))}
            </div>
          </div>
          <AddButton handleAddField={handleAddField} />
        </div>
        <div style={{marginBottom: "20px"}} className={"title"}>Description</div>
        <div style={{width: '100%', height: '350px', marginTop: '30px', marginBottom: '30px'}}>
          <TextArea name={"description"}  onChange={handleWhitelistChange} />
        </div>
        <ActionButton
          onClick={handleAddWhitelist}
          isDisable={isDisable}
          name={"ADD"}
        />
        <CloseIcon />

      </div>
      {isDisable ? <Preloader /> : ""}
    </div>
  );
};

export default AddWhitelist;
