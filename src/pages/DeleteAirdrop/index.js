import React, {useEffect, useState} from "react";
import {DeleteElement, NavBarAdmin} from "../../components";
import AirdropService from "../../services/airdropService";
import {useAuth} from "../../contexts/AuthProvider";

const DeleteAirdrop = () => {
    const airdropService = new AirdropService();

    const { userToken } = useAuth();

    const [airdrops, setAirdrops] = useState([]);

    const handleDeleteAirdrop = (id) => {
        setAirdrops(prev => prev.filter((airdrop) => airdrop.id !== id))
        airdropService.deleteAirdropById(id, userToken).then(() => {

        });
    }

    useEffect(() => {
        airdropService.getAllAirdrops().then((response) => {
            setAirdrops(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {airdrops?.map((airdrop) => (
                    <DeleteElement name={airdrop?.project} data={airdrop?.ends} id={airdrop?.id} handleDelete={handleDeleteAirdrop} />
                ))}
            </div>
        </div>
    )
}

export default DeleteAirdrop;