import React, {useEffect, useState} from "react";
import TagService from "../../services/tagService";
import NewsService from "../../services/newsService";
import {
    ActionButton,
    ActionTitle,
    CustomTag,
    Input,
    NavBarAdmin, Preloader,
    SourceInput,
    TagButton,
    TextArea
} from "../../components";
import {useAuth} from "../../contexts/AuthProvider";

const EditNews = ({match}) => {
    const tagService = new TagService();
    const newsService = new NewsService();

    const { userToken } = useAuth();

    const [isDisable, setIsDisable] = useState(false);
    const [tags, setTags] = useState([])
    const [selectedTags, setSelectedTags] = useState([]);

    const [news, setNews] = useState({
        title: "",
        text: "",
        tags: [],
        customTag: "",
        comments: [],
        photo: "",
        data: new Date().valueOf(),
        creatorId: "",
        source: "",
        creatorName: ""
    })
    const handleAddNews = () => {
        setIsDisable(true)
        newsService.editNewsById(match.params.newsId, news, userToken).then((response) => {
            setIsDisable(false)
        })
    }

    const handleNewsChange = (name, value) => {
        setNews((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };

    const handleAddTag = (tag) => {
        setSelectedTags(prev => !prev.includes(tag) ?  [...prev, tag] : prev.filter((item) => item !== tag))
    }

    useEffect(() => {
        newsService.getNews(match.params.newsId, userToken).then((response) => {
            setNews(response.data)
            setSelectedTags(response.data.tags)
        })
        tagService.getAllTags().then((response) => {
            setTags(response.data)
        })
    }, [userToken])
    useEffect(() => {
        handleNewsChange("tags", selectedTags)
    }, [selectedTags])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"EDIT NEWS"} actionType={"EDIT"} />
                <div style={{display: 'flex'}}>
                    <Input handleInputChange={handleNewsChange} name={"title"} label={"title"} value={news?.title}/>
                </div>
                <div style={{marginBottom: '60px', position: 'relative'}}>
                    <div>
                        <Input handleInputChange={handleNewsChange} name={"photo"} label={"photo/logo"} value={news?.photo} />
                    </div>
                    <div style={{marginBottom: "20px"}} className={"title"}>TAGS CLOUD</div>
                    <div style={{display: 'flex', flexWrap: 'wrap', maxWidth: '50%', marginBottom: '25px'}}>
                        {tags.map((item) => (
                            <TagButton addTag={handleAddTag} name={item} selectedTags={selectedTags} />
                        ))}
                        <CustomTag setCustomTag={handleNewsChange} name={"customTag"}/>
                    </div>
                    <div>
                        <div style={{marginBottom: "20px"}} className={"title"}>CONTENT</div>
                        <SourceInput onChange={handleNewsChange} name={"source"} />
                        <div style={{width: '100%', height: '350px', marginTop: '30px'}}>
                            <TextArea name={"text"} onChange={handleNewsChange} value={news?.text}/>
                        </div>
                    </div>
                </div>

                <ActionButton onClick={handleAddNews} isDisable={isDisable} name={"EDIT"}  />
            </div>
            {isDisable ? <Preloader /> : ""}

        </div>
    )
}
export default EditNews;