import React, { useState } from "react";
import {
  AuthenticationBlock,
  AuthenticationTitle,
  AuthenticationButton,
  AuthenticationInput,
  AuthenticationErrors,
} from "../../components";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../../contexts/AuthProvider";
import UserService from "../../services/userService";

const Register = () => {
  const { signUp, refreshMyInfo } = useAuth();

  const userService = new UserService();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  const [errors, setErrors] = useState("");
  const [userName, setUserName] = useState("");

  const history = useHistory();

  const handleRegisterUser = () => {
    setErrors("");
    if (password !== passwordConfirmation) {
      setErrors("Passwords are not the same!");
    }else if(userName === "" || email === "" || password === "" || passwordConfirmation === ""){
      setErrors("Fields must be filled!");
    }
    else {
      userService.checkUser(userName).then(() => {

      }).then(() => {
        signUp(email, password).then((response) => {
          if (response.user) {
            response.user.getIdToken(false).then((token) => {
              userService
                  .loginUser(token, userName)
                  .then((registerUser) => {
                    refreshMyInfo(registerUser.data)
                    history.push("/");
                  })
            });
          }
        }).catch((error) => {
          setErrors(error.message)
        });
      }).catch((error) => {
        if(error.message === "Request failed with status code 409")
          setErrors("User with this user name is already exists!")
      })

    }
  };

  return (
    <div>
      <AuthenticationBlock>
        <AuthenticationTitle title={"Log In"} />
        <AuthenticationInput
            onValueChange={setUserName}
            type={"text"}
            label={"User Name"}
        />
        <AuthenticationInput
          onValueChange={setEmail}
          type={"text"}
          label={"Email"}
        />
        <AuthenticationInput
          onValueChange={setPassword}
          type={"password"}
          label={"Password"}
        />
        <AuthenticationInput
          onValueChange={setPasswordConfirmation}
          type={"password"}
          label={"Password Confirmation"}
        />
        <AuthenticationButton
          onPress={() => handleRegisterUser()}
          text={"REGISTER"}
        />
        <AuthenticationErrors errors={errors} />
        <div className={"authentication-link-wrapper"}>
          <div>Already have an account?</div>
          <Link className={"link"} to={"/login"}>
            <div className={"authentication-link"}>Log In</div>
          </Link>
        </div>
      </AuthenticationBlock>
      <div className={"authentication-main"}>
        <Link to={"/"}>
          <img src={require("../../assets/icons/logo.svg").default} alt="" />
        </Link>
      </div>
    </div>
  );
};

export default Register;
