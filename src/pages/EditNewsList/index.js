import React, {useEffect, useState} from "react";
import { EditElement, NavBarAdmin} from "../../components";
import NewsService from "../../services/newsService";

const EditNewsList = () => {
    const newsService = new NewsService();

    const [news, setNews] = useState([]);


    useEffect(() => {
        newsService.getAllNews().then((response) => {
            setNews(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {news?.map((item) => (
                    <EditElement name={item?.title} data={item?.data} link={`edit-news/${item?.id}`}  />
                ))}
            </div>
        </div>
    )
}

export default EditNewsList;