import React from "react";
import {
    ActionButton,
    ActionTitle,
    Input,
    NavBarAdmin,
    TextArea
} from "../../components";

const AddResults = () => {
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"ADD RESULTS"} actionType={"ADD"} />
                <div >
                    <Input name={"PROJECT"} />
                    <Input name={"LINK"} />
                </div>
                <div style={{marginBottom: '90px', position: 'relative'}}>
                    <div>
                        <div style={{marginBottom: "20px"}} className={"title"}>DESCRIPTION</div>
                        <div style={{width: '500px', height: '200px', marginTop: '30px'}}>
                            <TextArea />
                        </div>
                    </div>
                </div>

                <ActionButton name={"ADD"} />
            </div>
        </div>
    )
}

export default AddResults;