import React, {useEffect, useState} from "react";
import AirdropService from "../../services/airdropService";
import { EditElement, NavBarAdmin} from "../../components";

const EditAirdropList = () => {
    const airdropService = new AirdropService();

    const [airdrops, setAirdrops] = useState([]);


    useEffect(() => {
        airdropService.getAllAirdrops().then((response) => {
            setAirdrops(response.data)
        })
    }, [])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                {airdrops?.map((airdrop) => (
                    <EditElement name={airdrop?.project} data={airdrop?.ends} link={`edit-airdrop/${airdrop?.id}`}  />
                ))}
            </div>
        </div>
    )
}

export default EditAirdropList;