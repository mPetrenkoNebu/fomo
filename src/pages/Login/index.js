import React, {useState} from "react";
import {
    AuthenticationBlock,
    AuthenticationTitle,
    AuthenticationButton,
    AuthenticationInput,
    AuthenticationErrors
} from "../../components";
import {Link, useHistory} from "react-router-dom";
import {useAuth} from "../../contexts/AuthProvider";
import UserService from "../../services/userService";

const Login = () => {
    const { login, refreshMyInfo } = useAuth();

    const userService = new UserService();

    const [errors, setErrors] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const history = useHistory();

    const handleLoginUser = () => {
        setErrors("")
        login(email, password).then((response) => {
            if (response.user) {
                response.user.getIdToken(false).then((token) => {
                    userService
                        .loginUser(token)
                        .then((registerUser) => {
                            refreshMyInfo(registerUser.data)
                            history.push("/");
                        });
                });
            }
        }).catch((errors) => {
            setErrors(errors.message)
        })
    }

    return (
        <div>
            <AuthenticationBlock>
                <AuthenticationTitle title={"Log In"} />
                <AuthenticationInput onValueChange={setEmail} type={"text"} label={"Email"} />
                <AuthenticationInput onValueChange={setPassword} type={"password"} label={"Password"}  />
                <AuthenticationButton onPress={handleLoginUser} text={"login"} />
                <AuthenticationErrors errors={errors} />
                <div className={"authentication-link-wrapper"}>
                    <div>Need an account?</div>
                    <Link className={"link"} to={"/register"}>
                        <div className={"authentication-link"}>Sign Up</div>
                    </Link>
                </div>

            </AuthenticationBlock>
            <div className={"authentication-main"}>
                <Link to={"/"}>
                    <img src={require('../../assets/icons/logo.svg').default} alt=""/>
                </Link>
            </div>
        </div>

    )
}

export default Login;