import React, {useEffect, useState} from "react";
import {
    ActionButton,
    SourceInput,
    ActionTitle,
    CustomTag,
    Input,
    NavBarAdmin,
    TextArea, TagButton, Preloader, ImagePicker, CloseIcon, AuthenticationErrors, PlusIcon
} from "../../components";
import TagService from "../../services/tagService";
import NewsService from "../../services/newsService";
import app from "../../firebase";
import {Toast} from "../../components/Toast/toast";
import {useHistory} from 'react-router-dom'
import {useAuth} from "../../contexts/AuthProvider";

const AddNews = () => {
    const { currentUser, userToken } = useAuth();

    const tagService = new TagService();
    const newsService = new NewsService();

    const history = useHistory()

    const [errors, setErrors] = useState("");
    const [isDisable, setIsDisable] = useState(false);
    const [tags, setTags] = useState([])
    const [image, setImage] = useState(null)
    const [selectedTags, setSelectedTags] = useState([]);
    const [customTags, setCustomTags] = useState([{id: 0, tag: ""}])
    const [file, setFile] = useState(null)

    const [news, setNews] = useState({
        title: "",
        text: "",
        tags: [],
        customTag: [],
        comments: [],
        photo: "",
        data: new Date().valueOf(),
        source: "",
        creatorId: currentUser.uid,
        creatorName: currentUser.name
    })


    const handleNewsChange = (name, value) => {
        setNews((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleAddCustomTag = () => {
        setCustomTags(prev => prev.concat({id: prev.length, tag: ""}))
    }
    const handleEditCustomTag = (id, value) => {
        setCustomTags(prev => prev.map((item) => {
            return item.id !== id ? item : {id: id, tag: value}
        }))
    }
    const handleChangeImage = (e) => {
        const file = e.target.files[0];
        if(file.type.startsWith("image")){
            setFile(URL.createObjectURL(file))
            setImage(file)
        }

    }
    const handleAddTag = (tag) => {
        setSelectedTags(prev => !prev.includes(tag) ?  [...prev, tag] : prev.filter((item) => item !== tag))
    }
    const handleUploadImage =async () => {
        const storageRef = app.storage().ref()
        const fileRef = storageRef.child(`news/${new Date().valueOf()}-${image.name}`)
        await fileRef.put(image)
        return await fileRef.getDownloadURL();
    }
    const isFieldsAreNotFilled = () => {
        return news.title === ""
            || news.text === ""
            || tags.length ===0
            || !image
    }
    const handleAddNews = async () => {
        setIsDisable(true)
        setErrors("")
        if(isFieldsAreNotFilled()){
            setErrors("All fields must be filled!");
            setIsDisable(false)
        }else{
            const imageUrl = await handleUploadImage()
            const newsTmp = {...news, ["photo"] : imageUrl}
            newsService.addNews(newsTmp, userToken).then(() => {
                setIsDisable(false)
                history.push('/admin')
                Toast.add({
                    text: "News are correctly added!",
                    color: "#6eaf0f",
                    autohide: true,
                    delay: 3000,
                });
            })
        }
    }

    useEffect(() => {
        tagService.getAllTags().then((response) => {
            setTags(response.data)
        })
    }, [])
    useEffect(() => {
        handleNewsChange("tags", selectedTags)
    }, [selectedTags])
    useEffect(() => {
        const customTagsTmp = customTags.reduce((prev, next) => {
            if(next.tag !== "")
                return prev.concat(next.tag)
            else return prev;
        }, [])
        handleNewsChange( "customTag", customTagsTmp)
    }, [customTags])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"ADD NEWS"} actionType={"ADD"} />
                {errors !== "" ? <AuthenticationErrors  errors={errors}/> : ""}
                <div style={{display: 'flex'}}>
                        <Input handleInputChange={handleNewsChange} name={"title"} label={"title"} />
                </div>
                <div style={{marginBottom: '60px', position: 'relative'}}>
                    <div>
                        <ImagePicker onChange={handleChangeImage} name={"Logo"} />
                        {file ? <img style={{maxHeight: '500px'}} src={file} alt=""/> : ""}

                    </div>
                    <div style={{marginBottom: "20px"}} className={"title"}>TAGS CLOUD</div>
                    <div style={{display: 'flex', flexWrap: 'wrap', maxWidth: '50%', marginBottom: '25px'}}>
                        {tags.map((item, index) => (
                              <TagButton key={item.tag.toString() + index} addTag={handleAddTag} name={item} selectedTags={selectedTags} />
                        ))}
                        {customTags.map((tag) => (
                            <CustomTag key={tag.id.toString()} setCustomTag={handleEditCustomTag} id={tag.id} />
                        ))}
                        <PlusIcon onClick={handleAddCustomTag} />
                    </div>
                    <div>
                        <div style={{marginBottom: "20px"}} className={"title"}>CONTENT</div>
                        <SourceInput onChange={handleNewsChange} name={"source"} />
                        <div style={{width: '100%', height: '350px', marginTop: '30px'}}>
                            <TextArea name={"text"} onChange={handleNewsChange} />
                        </div>
                    </div>
                </div>

                <ActionButton onClick={handleAddNews} isDisable={isDisable} name={"ADD"} />
                <CloseIcon />

            </div>
            {isDisable ? <Preloader /> : ""}

        </div>
    )
}

export default AddNews;