import React, {useEffect, useState} from "react";
import {
    ActionTitle,
    Input,
    InstructionSelect,
    NavBarAdmin,
    AddButton,
    ActionButton,
    InputDate,
    Preloader, Type, ImagePicker, CloseIcon, AuthenticationErrors, TextArea
} from "../../components";
import InstructionService from "../../services/instructionService";
import AirdropService from "../../services/airdropService";
import TypeService from "../../services/typeService";
import app from "../../firebase";
import {Toast} from "../../components/Toast/toast";
import {useHistory} from 'react-router-dom'
import {useAuth} from "../../contexts/AuthProvider";


const AddAirdrops = () => {
    const { currentUser, userToken } = useAuth();

    const airdropService = new AirdropService()
    const instructionService = new InstructionService();
    const typeService = new TypeService();

    const history = useHistory()

    const [errors, setErrors] = useState("");
    const [isDisable, setIsDisable] = useState(false);
    const [fields, setFields] = useState([]);
    const [types, setTypes] = useState([])
    const [selectedTypes, setSelectedTypes] = useState([])
    const [instructions, setInstructions] = useState([]);
    const [file, setFile] = useState(null)
    const [image, setImage] = useState(null)
    const [selectInstructions, setSelectInstructions] = useState([
        {id: 0, name: "", value: ""}
    ]);
    const [airdrop, setAirdrop] = useState({
        isFilled: false,
        iconUrl: "",
        project: "",
        reward: "",
        types: [],
        searchTypes: [],
        ends: "",
        description: "",
        status: "",
        creatorId: currentUser.uid,
        creatorName: currentUser.name
    })
    const handleAddField = () => {
        setFields(prev => [...prev, instructions])
    }
    const handleChangeImage = (e) => {
        const file = e.target.files[0];
        if(file.type.startsWith("image")){
            setFile(URL.createObjectURL(file))
            setImage(file)
        }
    }
    const handleAirdropChange = (name, value) => {
        setAirdrop((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleUploadImage =async () => {
        const storageRef = app.storage().ref()
        const fileRef = storageRef.child(`airdrops/${new Date().valueOf()}-${image.name}`)
        await fileRef.put(image)
        return await fileRef.getDownloadURL();
    }
    const isFieldsAreNotFilled = () => {
        return airdrop.project === ""
            || airdrop.description === ""
            || airdrop.ends === ""
            || airdrop.reward === ""
            || airdrop.description === ""
            || airdrop.types.length === 0
            || !image
    }
    const handleAddAirdrop = async () => {
        setIsDisable(true)
        setErrors("")
        if(isFieldsAreNotFilled()){
            setErrors("All fields must be filled!")
            setIsDisable(false)
        }else{
                const imageUrl = await handleUploadImage()
                const airdropTmp = {...airdrop, ["iconUrl"] : imageUrl}
                airdropService.addAirdrop(airdropTmp, userToken).then((response) => {
                    setIsDisable(false)
                    history.push('/admin')
                    Toast.add({
                        text: "Airdrop are correctly added!",
                        color: "#6eaf0f",
                        autohide: true,
                        delay: 3000,
                    });
                })
            }
    }

    const handleAddInstruction = (index, instruction, value) => {
        setSelectInstructions(prev => prev.map((item) => item.id === index ? {
            id: index, name: instruction, value: value
        } : item))
    }
    useEffect(() => {
        instructionService.getInstructions().then((response) => {
            setInstructions(response.data)
            setFields([response.data])
        })
        typeService.getAllTypes().then((response) => {
            setTypes(response.data)
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const handleChangeSelectedTypes = (type) => {
        setSelectedTypes(prev => prev.find((item) => item.name === type.name) ?prev.filter((item) => item.name !== type.name) : prev.concat(type)  )
    }
    useEffect(() => {
        if(fields.length !== 1 && fields.length !== 0){
            setSelectInstructions(prev => [...prev, {id: prev.length, name: "", value: ""}])
        }
    },  [fields])
    useEffect(() => {
        handleAirdropChange("instructions", selectInstructions)
    }, [selectInstructions])
    useEffect(() => {
        handleAirdropChange("types", selectedTypes)
        handleAirdropChange("searchTypes", selectedTypes.reduce((prev, next) => {
            return prev.concat(next.name)
        }, []))

    }, [selectedTypes])
    return (
        <div>
            <NavBarAdmin isBack={false} />
            <div className={"container"}>
                <ActionTitle name={"ADD AIRDROP"} actionType={"ADD"} />
                {errors !== "" ? <AuthenticationErrors  errors={errors}/> : ""}
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: '75px'}}>
                        <Input handleInputChange={handleAirdropChange} name={"project"} label={"project"} />
                        <Input handleInputChange={handleAirdropChange} name={"website"} label={"website"} />
                        <ImagePicker onChange={handleChangeImage} name={"Logo"} />
                    </div>
                    <div>
                        <Input handleInputChange={handleAirdropChange} name={"reward"}  label={"reward"}/>
                        <InputDate handleInputChange={handleAirdropChange}  name={"starts"}  label={"starts"} />
                        <InputDate handleInputChange={handleAirdropChange}  name={"ends"}  label={"ends"} />
                    </div>
                </div>
                {file ? <img style={{maxHeight: '500px'}} src={file} alt=""/> : ""}
                <div style={{marginBottom: '90px', position: 'relative'}}>
                    <div style={{marginBottom: "20px"}} className={"title"}>Types</div>
                    <div style={{display: 'flex', marginBottom: '15px'}}>
                        {types?.map((type) => (
                            <Type key={type.id} type={type} onClick={handleChangeSelectedTypes} selectedTypes={selectedTypes} />
                        ))}
                    </div>

                    <div style={{marginBottom: "20px"}} className={"title"}>Instruction</div>
                    <div>
                        <div>
                            {fields.map((item, index) => (
                                <InstructionSelect onChange={handleAddInstruction} items={item} index={index}/>
                            ) )}
                        </div>
                    </div>
                    <AddButton handleAddField={handleAddField} />
                </div>
                <div style={{marginBottom: "20px"}} className={"title"}>Description</div>
                <div style={{width: '100%', height: '350px', marginTop: '30px', marginBottom: '30px'}}>
                    <TextArea name={"description"}  onChange={handleAirdropChange} />
                </div>
                <ActionButton onClick={handleAddAirdrop} isDisable={isDisable} name={"ADD"} />
                <CloseIcon />
            </div>
            {isDisable ? <Preloader /> : ""}

        </div>
    )
}

export default AddAirdrops;