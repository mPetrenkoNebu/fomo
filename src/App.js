import React from 'react'
import {Router} from "./components";
import {Nebu} from "./components";

function App() {
  return (
    <>
      <Router />
      <Nebu />
    </>
  );
}

export default App;
