import React, { useContext, useState } from "react";
import  { auth } from "../../firebase";
import UserService from "../../services/userService";

const AuthContext = React.createContext({});

export function useAuth() {
    return useContext(AuthContext);
}

const AuthProvider = ({ children }) => {
    const userService = new UserService();

    const [currentUser, setCurrentUser] = useState();
    const [loading, setLoading] = useState(true);
    const [userToken, setUserToken] = useState(null);

    const signUp = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password)
    };
    const refreshMyInfo = async (user) => {
        setCurrentUser(user);
    };
    const login = (email, password) => {
        return auth.signInWithEmailAndPassword(email, password)
    };
    const logout = () => {
        return auth.signOut();
    };
    const resetPassword = (email) => {
        return auth.sendPasswordResetEmail(email)
    }
    React.useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((user) => {
            if(user){
                setCurrentUser(user);
                user.getIdToken(false).then((token) => {
                    setUserToken(token)
                    userService
                        .loginUser(token)
                        .then((registerUser) => {
                            refreshMyInfo(registerUser.data)
                        });
                })
            }else{
                setCurrentUser(null)
            }
            setLoading(false);

        });
        return unsubscribe;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    const value = {
        currentUser,
        userToken,
        signUp,
        login,
        logout,
        resetPassword,
        refreshMyInfo,
    };
    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;
