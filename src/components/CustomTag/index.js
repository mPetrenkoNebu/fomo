import React from "react";

const CustomTag = ({setCustomTag, id}) => {
    return (
        <div className={"custom-input-wrapper"}>
            <input onChange={(e) => setCustomTag(id, e.target.value)} placeholder={"ADD CUSTOM"} className={"custom-input"} type="text"/>
        </div>
    )
}

export default CustomTag;