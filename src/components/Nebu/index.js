import React from "react";

const Nebu = () => {
    return (
        <a className={"link"} target="_blank" href="https://www.nebucode.com/">
            <div className={"nebu-wrapper"}>
                <div className={"nebu-text"}>©2021 Made with</div>
                <div className={"nebu-logo-wrapper"}>
                    <img className={"nebu-logo"} src={require('../../assets/icons/nebulogo.svg').default} alt=""/>
                </div>
                <div className={"nebu-text"}>by Nebucode</div>
            </div>
        </a>
    )
}

export default Nebu;