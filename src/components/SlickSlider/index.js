import React from "react";
import Slider from "react-slick";

const SlickSlider = ({ children, settings }) => {
  return (
    <div>
        <Slider {...settings}>{children}</Slider>
    </div>
  );
};

export default SlickSlider;
