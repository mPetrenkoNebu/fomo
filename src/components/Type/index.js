import React from "react";

const Type = ({type, onClick, selectedTypes}) => {
    return (
        <div onClick={() => onClick(type)} style={{color: `#${type.color}`}} className={`type ${selectedTypes.find((item) => item.name === type.name) ? "type--active" : ""}`}>
            {type.name}
        </div>
    )
}

export default Type;