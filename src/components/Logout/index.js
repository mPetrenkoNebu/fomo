import React from "react";
import { useAuth } from "../../contexts/AuthProvider";
import { useHistory } from "react-router-dom";

const Logout = () => {
  const { logout } = useAuth();
  const history = useHistory();

  const handleLogOut = () => {
    logout().then((response) => {
      history.push("/");
    });
  };
  return (
    <div onClick={() => handleLogOut()} className={"logout"}>
      Log Out
    </div>
  );
};

export default Logout;
