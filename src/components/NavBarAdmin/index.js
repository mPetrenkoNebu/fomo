import React from "react";
import Navigation from "./Navigation";
import Logo from "../Logo";
import User from "../User";

const NavBarAdmin = ({isBack, setIsNavOpen, isNavOpen}) => {
    return (
        <div className={"nav-admin-wrapper"}>
            <div className={"nav-admin-container"}>
                <Logo setIsNavOpen={setIsNavOpen} isNavOpen={isNavOpen}/>
                <Navigation isBack={isBack} />
            </div>
            <User />
        </div>
    )
}

export default NavBarAdmin;