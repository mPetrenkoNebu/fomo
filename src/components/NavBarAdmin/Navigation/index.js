import React from "react";
import Item from "./Item";
import { NavLink, useHistory } from "react-router-dom";

const Navigation = ({ isBack }) => {
  const history = useHistory();
  const handleBack = () => {
    history.push(`/`);
  };
  return (
    <div className={"nav-admin"}>
      <div className={"nav-admin-items"}>
        <NavLink
          to={"/admin"}
          activeClassName={"nav-admin-active"}
          className={"link nav-admin-link"}
          // exact
        >
          <Item name={"Admin"} />
        </NavLink>
      </div>
      {isBack ? (
        <div onClick={() => handleBack()} className={"nav-admin-back"}>
          >BACK
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default Navigation;
