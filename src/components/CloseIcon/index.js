import React from "react";
import { useHistory } from 'react-router-dom'

const CloseIcon = () => {
    const history = useHistory();

    return (
        <div className={"close-icon"} onClick={() => history.push('/admin')}>
            <img className={"close-icon-image"} src={require('../../assets/icons/close-icon.svg').default} alt=""/>
        </div>
    )
}

export default CloseIcon;