import React from "react";
import {
  AddAirdrops,
  AddNews,
  AddResults,
  AddWhitelist,
  Admin,
  Airdrop,
  Airdrops,
  Calendar,
  DeleteAirdrop,
  DeleteNews,
  DeleteWhitelist,
  EditAirdrop,
  EditAirdropList,
  EditNews,
  EditNewsList,
  EditWhitelist,
  Login,
  Main,
  News,
  Register, Tools,
  Whitelist,
  Whitelists,
} from "../../pages";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PrivateRoute from "../PrivateRoute";
import AuthProvider from "../../contexts/AuthProvider";
import EditWhitelistList from "../../pages/EditWhitelistList";
import PrivateRouteAdmin from "../PrivateRouteAdmin";

const Router = () => {
  return (
    <BrowserRouter>
      <AuthProvider>
        <Switch>
          <Route exact path={["/", "/news"]} component={Main} />
          <PrivateRoute exact path={"/news/:newsId"} component={News} />
          <Route exact path={"/whitelists"} component={Whitelists} />
          <PrivateRoute
            exact
            path={"/whitelists/:whitelistId"}
            component={Whitelist}
          />
          <PrivateRoute exact path={"/airdrops/:airdropId"} component={Airdrop} />
          <Route exact path={"/airdrops"} component={Airdrops} />
          <Route exact path={"/tools"} component={Tools} />
          <Route exact path={"/calendar"} component={Calendar} />
          <Route exact path={"/login"} component={Login} />
          <Route exact path={"/register"} component={Register} />
          <PrivateRouteAdmin exact path={"/admin"} component={Admin} />
          {/*ADD  ROUTES*/}
          <PrivateRouteAdmin exact path={"/admin/add-airdrops"} component={AddAirdrops}/>
          <PrivateRouteAdmin exact path={"/admin/add-whitelist"} component={AddWhitelist} />
          <PrivateRouteAdmin exact path={"/admin/add-news"} component={AddNews} />
          <PrivateRouteAdmin exact path={"/admin/add-results"} component={AddResults}/>
          {/*DELETE ROUTES*/}
          <PrivateRouteAdmin exact path={"/admin/delete-whitelist"} component={DeleteWhitelist} />
          <PrivateRouteAdmin exact path={"/admin/delete-airdrop"} component={DeleteAirdrop} />
          <PrivateRouteAdmin exact path={"/admin/delete-news"} component={DeleteNews}/>
          {/*EDIT ROUTES*/}
          <PrivateRouteAdmin exact path={"/admin/edit-whitelist"} component={EditWhitelistList} />
          <PrivateRouteAdmin exact path={"/admin/edit-whitelist/:whitelistId"} component={EditWhitelist} />
          <PrivateRouteAdmin exact path={"/admin/edit-airdrop"} component={EditAirdropList} />
          <PrivateRouteAdmin exact path={"/admin/edit-airdrop/:airdropId"} component={EditAirdrop} />
          <PrivateRouteAdmin exact path={"/admin/edit-news"} component={EditNewsList} />
          <PrivateRouteAdmin exact path={"/admin/edit-news/:newsId"} component={EditNews} />
        </Switch>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Router;
