import React from "react";
import {Link} from "react-router-dom";

const EditAction = ({action, link}) => {
    return (
        <Link className={"link"} to={link}>
            <div className={"action"}>
                <div style={{width: '7px', height: '7px', marginRight: '8px'}} className={"action-icon-wrapper"}>
                    <img className={"action-icon"} src={require('../../assets/icons/edit-icon.svg').default} alt=""/>
                </div>
                <div className={"action-text"}>{action}</div>
            </div>
        </Link>
    )
}

export default EditAction;