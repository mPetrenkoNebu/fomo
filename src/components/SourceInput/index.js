import React from "react";

const SourceInput = ({onChange, name}) => {
    return (
        <div >
            <input onChange={(e) => onChange(name, e.target.value)} placeholder={"SOURCE (IF APPLICABLE)"} className={"source-input"} type="text"/>
        </div>
    )
}

export default SourceInput;