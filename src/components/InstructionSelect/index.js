import React, {useEffect, useState} from "react";

const InstructionSelect = ({ items, index, itemLength, onChange, instruction }) => {
  const [selectOption, setSelectOption] = useState("");
  const [customSelectOption, setCustomSelectOption] = useState("")
  const [input, setInput] = useState("")
  const [isVisible, setIsVisible] = useState(false);
  const handleOpenSelect = (e) => {
      if(e.target.tagName !== "INPUT")
    setIsVisible(true);
  };
  const handleSelectItem = (value) => {
    setSelectOption(value);
      if(value.name !== "Custom"){
          onChange(index, value.name, input)
      }else{
          onChange(index, customSelectOption, input)
      }
    setIsVisible(false);
  };
  useEffect(() => {
      handleSelectItem(selectOption)
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [input])
  return (
    <div className={"instruction-select"}>
      <div
        onClick={(e) => handleOpenSelect(e)}
        className={"instruction-select-value"}
      >
        <div className={"instruction-select-item-wrapper"}>
            <div className={"instruction-select-item"}>{instruction?.name !== ""  && selectOption === "" && instruction?.name !== undefined  ?instruction?.name : selectOption?.name}</div>
            <div className={"instruction-select-item-wrapper--custom"}  style={{display: selectOption?.name !== "Custom" ? 'none' : 'block'}}>
                <input onChange={(e) => setCustomSelectOption(e.target.value)} placeholder={"Custom"}  className={"instruction-select-input--custom"} type="text"/>
            </div>
        </div>
        <div className={"instruction-select-arrow-wrapper"}>
          <img
            className={"instruction-select-arrow"}
            src={require("../../assets/icons/arrow-down-icon.svg").default}
            alt=""
          />
        </div>
      </div>
      <div className={"instruction-select-wrapper"}>
        <input
          className={"instruction-select-input"}
          placeholder={"PASTE LINK"}
          onChange={(e) => setInput(e.target.value)}
          type="text"
          value={instruction?.value}
        />
      </div>
      <div
        style={{ display: isVisible ? "block" : "none" }}
        className={"instruction-select-modal"}
      >
        {items.map((item) => (
          <div
            onClick={() => handleSelectItem(item)}
            className={"instruction-select-modal-item-wrapper"}
            key={item.id}
          >
            <div className={"instruction-select-modal-item"}>{item.name} </div>
          </div>
        ))}
      </div>
      <div className={"instruction-select-index"}>{`${index + 1}.`}</div>
    </div>
  );
};

export default InstructionSelect;
