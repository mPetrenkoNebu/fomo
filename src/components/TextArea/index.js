import React, { useState} from "react";

const TextArea = ({onChange, name, value}) => {
    const [valueInput, setValueInput] = useState("")
    const handleChangeInput = (text) => {
        setValueInput(text)
        onChange(name, text.replaceAll("\n", "*!brk!*"))
    }
    return (
        <textarea  value={valueInput} onChange={(e) => handleChangeInput( e.target.value)} className={"textarea"}/>
    )
}

export default TextArea;