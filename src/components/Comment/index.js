import React from "react";
import {romanMonths} from "../../data/romanMonths";

const Comment = ({author, comment, data}) => {
    const getDate = () => {
        const tmpDate = new Date(data);
        return `${tmpDate.getDate()}.${romanMonths[tmpDate.getMonth()]}.${tmpDate.getFullYear()}`
    }
    return (
        <div className={"comment-container"}>
            <div className={"comment"}>
                <div className={"comment--wrapper"}>
                    <div className={"comment-author"}>{author}</div>
                    <div className={"comment-text"}>{comment}</div>
                </div>
                <div>{getDate()}</div>
            </div>
            <div className={"comment-separator"} />
        </div>

    )
}

export default Comment;