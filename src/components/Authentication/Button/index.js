import React from "react";

const Button = ({text, onPress}) => {
    return (
        <div onClick={() => onPress()} className={"authentication-button"}>
            {text}
        </div>
    )
}

export default Button;