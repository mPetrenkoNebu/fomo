import React from "react";

const Block = ({children}) => {
    return (
        <div className={"authentication-block"}>
            {children}
        </div>
    )
}

export default Block;