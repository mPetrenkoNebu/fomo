import React from "react";

const Errors = ({errors}) => {
    return (
        <div className={"authentication-error"}>
            {errors}
        </div>
    )
}

export default Errors;