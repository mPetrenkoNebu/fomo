import React from "react";

const Input = ({type, label, onValueChange}) => {
    return (
        <div className={"authentication-input-wrapper"}>
            <div className={"authentication-label"}>{label}</div>
            <input onChange={(e) => onValueChange(e.target.value)}  className={"authentication-input"} type={type} />
        </div>
    )
}

export default Input;