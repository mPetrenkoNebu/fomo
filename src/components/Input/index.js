import React from "react";

const Input = ({name, label, handleInputChange, value}) => {
    return (
        <div>
            <div className={"input-name"}>{label}</div>
            <div className={"input-wrapper"}>
                <input value={value} onChange={(e) => handleInputChange(name, e.target.value)} className={"input"} type="text"/>
            </div>
        </div>
    )
}

export default Input;