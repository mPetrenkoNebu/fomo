import React from "react";

const Info = ({items}) => {
    return (
        <div className={"info"}>
            <div>
                {items.map((item) => (
                    <div key={item.key} className={"info-text"}>{item.key}: {item.value}</div>
                ))}
            </div>
            <div className={"info-button"}>
                results
            </div>
        </div>
    )
}

export default Info;