import React from "react";
import { romanMonths } from "../../data/romanMonths";

const DeleteElement = ({ name, data, id, handleDelete }) => {
  const getDate = () => {
    const tmpDate = new Date(data);
    return `${tmpDate.getDate()}.${
      romanMonths[tmpDate.getMonth()]
    }.${tmpDate.getFullYear()}`;
  };

  return (
    <div className={"delete-element"}>
      <div>{name}</div>
      <div>{getDate()}</div>
      <img onClick={() => handleDelete(id)} className={"delete-element-button"} src={require("../../assets/icons/minus-icon.svg").default} alt="" />
    </div>
  );
};

export default DeleteElement;
