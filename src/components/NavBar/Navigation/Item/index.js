import React from "react";

const Item = ({name}) => {
    return (
        <div className={"nav-item"}>
            {name}
        </div>
    )
}

export default Item;