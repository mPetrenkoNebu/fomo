import React from "react";
import Item from "./Item";
import { NavLink } from "react-router-dom";
import { useAuth } from "../../../contexts/AuthProvider";

const Navigation = () => {
  const { currentUser } = useAuth();
  return (
    <div>
      <div className={"nav-items"}>
        <NavLink
          to={"/"}
          activeClassName={"nav-active"}
          className={"link nav-link"}
          exact
        >
          <Item name={"News"} />
        </NavLink>
        <NavLink
          to={"/calendar"}
          activeClassName={"nav-active"}
          className={"link nav-link"}
          exact
        >
          <Item name={"Calendar"} />
        </NavLink>
        <NavLink
          to={"/whitelists"}
          activeClassName={"nav-active"}
          className={"link nav-link"}
          exact
        >
          <Item name={"Whitelists"} />
        </NavLink>
        <NavLink
          to={"/airdrops"}
          activeClassName={"nav-active"}
          className={"link nav-link"}
          exact
        >
          <Item name={"Airdrops"} />
        </NavLink>
        {currentUser && currentUser?.isAdmin ? (
          <NavLink
            to={"/admin"}
            activeClassName={"nav-active"}
            className={"link nav-link"}
            exact
          >
            <Item name={"Admin"} />
          </NavLink>
        ) : (
          ""
        )}

        {/*<NavLink*/}
        {/*  to={"/tools"}*/}
        {/*  activeClassName={"nav-active"}*/}
        {/*  className={"link nav-link"}*/}
        {/*  exact*/}
        {/*>*/}
        {/*  <Item name={"Tools"} />*/}
        {/*</NavLink>*/}
      </div>
    </div>
  );
};

export default Navigation;
