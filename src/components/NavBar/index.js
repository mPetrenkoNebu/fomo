import React from "react";
import Navigation from "./Navigation";
import User from "../User";
import Logo from "../Logo";

const NavBar = ({isNavOpen, setIsNavOpen}) => {
    return (
        <div className={"nav-wrapper"}>
            <div className={"nav-container"}>
                <Logo setIsNavOpen={setIsNavOpen} isNavOpen={isNavOpen}/>
                <Navigation />
            </div>
            <User />
        </div>
    )
}


export default NavBar;