import React from "react";

const AddButton = ({handleAddField}) => {
    return (
        <div onClick={() => handleAddField()} className={"add-button"}>
            +
        </div>
    )
}

export default AddButton;