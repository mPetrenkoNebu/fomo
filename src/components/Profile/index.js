import React, {useEffect, useState} from "react";
import Select from "../Select";
import Item from "./Item";
import {useAuth} from "../../contexts/AuthProvider";
import UserService from "../../services/userService";

const Profile = () => {
    const { currentUser, userToken, refreshMyInfo } = useAuth();

    const userService = new UserService();

    const [instructions, setInstructions] = useState(null)
    const [selectedProfile, setSelectedProfile] = useState(null)
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [isFirstOpen, setIsFirstOpen] = useState(true)
    const [selectedIndex, setSelectedIndex] = useState(0)

    const handleChangeInstruction = (instruction, value, indexItem) => {
        setInstructions(prev => prev?.map((item, index) => {
            return index !== indexItem ? item : {name: selectedProfile.name, items: item?.items?.map((instr) => {
                return instr?.name !== instruction ? instr : {name: instruction, value: value}
            })
        }
        }))
    }
    const handleAddProfile =  () => {
        const profile = {
            name: `Profile ${instructions.length + 1}`, items: [{name: "Telegram", value: ""}, {name: "Twitter", value: ""}, {name: "ETH", value: ""}]
        }
        setInstructions(prev => prev.concat(profile))
        setSelectedProfile(profile)
        setSelectedIndex(instructions.length + 1)
    }
    const handleChangeProfile = (profile, index) => {
        setSelectedProfile(profile)
        setSelectedIndex(index)
        setIsModalOpen(false)
    }
    useEffect(() => {
        if(instructions){
            userService.setInstructions(instructions, userToken).then((response) => {
                refreshMyInfo(response.data)
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [instructions])
    useEffect(() => {
        if(currentUser?.instructions && isFirstOpen){
            setInstructions(currentUser.instructions)
            setSelectedProfile(currentUser.instructions[0])
            setIsFirstOpen(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser])
    return (
        <div className={"profile"}>
            <div className={"profile-main"}>
                <Select onClick={() => setIsModalOpen(true)} text={selectedProfile?.name} />
                <div onClick={() => handleAddProfile()} className={"profile-plus"}>
                    +
                </div>
                <div style={{display: isModalOpen ? "block" : "none"}} className={"profile-modal"}>
                    {instructions?.map((item, index) => (
                        <div onClick={() => handleChangeProfile(instructions[index], index)} className={"profile-modal-item-wrapper"}>
                            <div className={"profile-modal-item"}>{item?.name}</div>
                        </div>
                    ))}
                </div>
            </div>
            <div>
                {selectedProfile && selectedProfile.items?.map((instruction) => (
                    <Item handleChangeInstruction={handleChangeInstruction} name={instruction.name} value={instruction.value} index={selectedIndex}/>
                ))}
            </div>
        </div>
    )
}

export default Profile;