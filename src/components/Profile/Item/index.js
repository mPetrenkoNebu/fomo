import React, { useState } from "react";
import Select from "../../Select";

const Item = ({ name, value, handleChangeInstruction, index }) => {
  const [copiedLink, setCopiedLink] = useState("");
  const handleCopy = () => {
    navigator.clipboard.writeText(value);
    setCopiedLink(value);
    setTimeout(() => {
      setCopiedLink("");
    }, 1000);
  };
  return (
    <div className={"profile-item"}>
      <div className={"profile-name"}>
        Your {name === "ETH" ? `${name} address` : `${name} @`}
      </div>
      <div className={"profile-select-wrapper"}>
        <div onClick={() => handleCopy()} className={"profile-icon-wrapper"}>
          <img
            className={"profile-icon"}
            src={require("../../../assets/icons/copy-icon.svg").default}
            alt=""
          />
          {copiedLink === value && value !== "" ? (
            <div className={"profile-copy"}>Copied!</div>
          ) : (
            ""
          )}
        </div>
        <div className={"profile-select"}>
          <Select handleChangeInstruction={handleChangeInstruction} text={value} name={name} index={index} />
        </div>
      </div>
    </div>
  );
};

export default Item;
