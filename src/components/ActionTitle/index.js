import React from "react";

const ActionTitle = ({ actionType, name }) => {
  return (
    <div className={"actiontitle"}>
      <div className={"actiontitle-icon-wrapper actiontitle-icon-wrapper--big"}>
        <img
          className={"actiontitle-icon"}
          src={
            actionType === "ADD"
              ? require("../../assets/icons/plus-icon.svg").default
              : actionType !== "EDIT"
              ? require("../../assets/icons/minus-icon.svg").default
              : require("../../assets/icons/edit-icon.svg").default
          }
          alt=""
        />
      </div>
        <div  className={"actiontitle-wrapper"}>
            <div className={"actiontitle-icon-wrapper"}>
                <img
                    className={"actiontitle-icon"}
                    src={
                        actionType === "ADD"
                            ? require("../../assets/icons/plus-icon.svg").default
                            : actionType !== "EDIT"
                            ? require("../../assets/icons/minus-icon.svg").default
                            : require("../../assets/icons/edit-icon.svg").default
                    }
                    alt=""
                />
            </div>
            <div  className={"actiontitle-name"}>{name}</div>
        </div>
    </div>
  );
};

export default ActionTitle;
