import React, {useEffect, useState} from "react";

const InputDate = ({name, label, handleInputChange, value}) => {
    const [minDate, setMinDate] = useState("");
    const [maxDate, setMaxDate] = useState("");

    const dateChange = (e) => {
        handleInputChange(name, new Date(e.target.value).valueOf())
    }
    const getCorrectNumberOfDate = (date) => {
        if(date < 10)
            return `0${date}`
        else return date
    }
    const getPeriodOfSelectedDates = () => {
        const currentDay = new Date();
        const minDate = `${currentDay.getFullYear() - 1}-${getCorrectNumberOfDate(currentDay.getMonth())}-${getCorrectNumberOfDate(currentDay.getDate())}`
        const maxDate = `${currentDay.getFullYear() + 14}-${getCorrectNumberOfDate(currentDay.getMonth())}-${getCorrectNumberOfDate(currentDay.getDate())}`
        setMinDate(minDate);
        setMaxDate(maxDate)
    }
    useEffect(() => {
        getPeriodOfSelectedDates()
    }, [])

    return (
        <div>
            <div className={"input-name"}>{label}</div>
            <div className={"input-wrapper"}>
                <input value={value} onChange={(e) => dateChange(e)} className={"input"} min={"2019-05-03"} max={maxDate} type="date"/>
            </div>
        </div>
    )
}

export default InputDate;