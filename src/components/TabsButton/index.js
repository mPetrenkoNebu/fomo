import React from "react";

const TabsButton = ({openInSeparateTabs}) => {
    return (
        <div onClick={() => openInSeparateTabs()} className={"tabs-button"}>
            <div className={"tabs-button-image-wrapper"}>
                <img  className={"tabs-button-image"} src={require('../../assets/icons/next-icon.svg').default} alt=""/>
            </div>
            <div  className={"tabs-button-text"}>
                Open in separate tabs
            </div>
        </div>
    )
}

export default TabsButton;