import React from "react";

const Title = ({ icon, name }) => {
  return (
    <div className={"title"}>
      <div className={"title-icon--wrapper"}>
        {icon === "" ? (
          <div className={"title-icon"} />
        ) : (
          <img className={"title-icon"} src={icon} alt="" />
        )}
      </div>
      <div className={"title-name"}>{name}</div>
    </div>
  );
};

export default Title;
