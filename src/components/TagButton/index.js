import React from "react";

const Tag = ({name, addTag, selectedTags}) => {
    return (
        <div onClick={() => addTag(name.tag)} className={`tag ${selectedTags.includes(name.tag) ? "tag--selected" : ""}`}>
            #{name.tag}
        </div>
    )
}

export default Tag;