import React, {useEffect, useState} from "react";
import useDebounce from "../../hooks/Debounce";

const Select = ({ text, handleChangeInstruction, name, onClick, index }) => {
    const [value, setValue] = useState(text);
    const [isFirstInput, setIsFirstInput] = useState(true)
    const debouncedValue = useDebounce(value, 2000);

    const handleChangeInput = (value) => {
        setIsFirstInput(false);
        setValue(value)
    }

    useEffect(() => {
        if(handleChangeInstruction)
             handleChangeInstruction(name, value, index)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [debouncedValue])

    useEffect(() => {
        setIsFirstInput(true)
    }, [index])
  return (
    <div onClick={() => onClick ? onClick() : {}} className={"select-wrapper"}>
      <div className={"select-icon-wrapper"}>
        <img
          className={"select-icon"}
          src={require("../../assets/icons/arrow-down-icon.svg").default}
          alt=""
        />
      </div>
      {text?.includes("Profile") || text?.includes("PROFILE") ? (
        <div className={"select-text"}>{text}</div>
      ) : (
        <input onChange={(e) => handleChangeInput(e.target.value)} value={isFirstInput ? text : value} className={"select-text"} />
      )}
    </div>
  );
};

export default Select;
