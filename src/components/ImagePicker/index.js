import React from "react";

const ImagePicker = ({name, onChange}) => {
    return (
        <div>
            <div className={"image-picker-name"}>{name}</div>
            <div className={"image-picker-wrapper"}>
                <input onChange={onChange}  className={"image-picker-custom"} type="file"/>
                <span className={"image-picker-span"}>Choose file</span>
            </div>
        </div>
    )
}

export default ImagePicker;