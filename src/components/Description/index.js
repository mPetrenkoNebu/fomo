import React from "react";

const Description = ({description}) => {
    return (
        <div className={"description"}>
            <div className={"description-title"}>Description</div>
            <div className={"description-text"}>{description}</div>
        </div>
    )
}

export default Description;