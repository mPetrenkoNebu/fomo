import React from "react";
import {Link} from "react-router-dom";

const AddAction = ({action, link}) => {
    return (
        <Link className={"link"} to={link}>
            <div className={"action"}>
                <div className={"action-icon-wrapper"}>
                    <img className={"action-icon"} src={require('../../assets/icons/plus-icon.svg').default} alt=""/>
                </div>
                <div className={"action-text"}>{action}</div>
            </div>
        </Link>

    )
}

export default AddAction;