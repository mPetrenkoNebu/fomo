import React from "react";

const PlusIcon = ({onClick}) => {
    return (
        <div onClick={() => onClick()} className={"plus-icon-wrapper"}>
            <div  className={"plus-icon"}>
                +
            </div>
        </div>
    )
}

export default PlusIcon;