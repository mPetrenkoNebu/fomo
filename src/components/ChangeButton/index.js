import React from "react";

const ChangeButton = ({handleSearch}) => {
    return (
        <div onClick={() => handleSearch()} className={"change-button"}>
            Change
        </div>
    )
}

export default ChangeButton;