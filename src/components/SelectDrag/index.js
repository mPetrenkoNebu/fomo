import React from "react";

const SelectDrag = () => {
  return (
    <div className={"instruction-select"}>
      <div className={"instruction-select-value"}>
        <div className={"instruction-select-item-wrapper"}></div>
        <div className={"instruction-select-arrow-wrapper"}>
          <img
            className={"instruction-select-arrow"}
            src={require("../../assets/icons/arrow-down-icon.svg").default}
            alt=""
          />
        </div>
      </div>
      <div className={"instruction-select-wrapper"}>
        <input
          className={"instruction-select-input"}
          placeholder={"PASTE LINK"}
          type="text"
        />
      </div>
    </div>
  );
};

export default SelectDrag;
