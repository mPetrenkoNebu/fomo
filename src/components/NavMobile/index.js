import React from "react";
import {NavLink} from "react-router-dom";
import Item from "../NavBar/Navigation/Item";
import {useAuth} from "../../contexts/AuthProvider";

const NavMobile = ({isNavOpen}) => {
    const { currentUser } = useAuth();

    return (
        <div style={{display: isNavOpen ? "block" : "none"}} className={"nav-mobile"}>
            <div className={"nav-mobile-wrapper"}>
                <div  className={"nav-mobile-container"}>
                    <NavLink
                        to={"/"}
                        activeClassName={"nav-active"}
                        className={"link nav-link"}
                        exact
                    >
                        <Item name={"News"} />
                    </NavLink>
                    <NavLink
                        to={"/calendar"}
                        activeClassName={"nav-active"}
                        className={"link nav-link"}
                        exact
                    >
                        <Item name={"Calendar"} />
                    </NavLink>
                    <NavLink
                        to={"/whitelists"}
                        activeClassName={"nav-active"}
                        className={"link nav-link"}
                        exact
                    >
                        <Item name={"Whitelists"} />
                    </NavLink>
                    <NavLink
                        to={"/airdrops"}
                        activeClassName={"nav-active"}
                        className={"link nav-link"}
                        exact
                    >
                        <Item name={"Airdrops"} />
                    </NavLink>
                    {currentUser && currentUser?.isAdmin ? (
                        <NavLink
                            to={"/admin"}
                            activeClassName={"nav-active"}
                            className={"link nav-link"}
                            exact
                        >
                            <Item name={"Admin"} />
                        </NavLink>
                    ) : (
                        ""
                    )}
                    {/*<NavLink*/}
                    {/*    to={"/tools"}*/}
                    {/*    activeClassName={"nav-active"}*/}
                    {/*    className={"link nav-link"}*/}
                    {/*    exact*/}
                    {/*>*/}
                    {/*    <Item name={"Tools"} />*/}
                    {/*</NavLink>*/}
                </div>
            </div>
        </div>
    )
}

export default NavMobile;