import React from "react";
import logo from "../../assets/icons/logo.svg";
import { Link } from "react-router-dom";
import Sandwich from "../Sandwich";

const Logo = ({setIsNavOpen, isNavOpen}) => {
  return (
    <div className={"nav-logo--container"}>

      <Sandwich setIsNavOpen={setIsNavOpen} isNavOpen={isNavOpen} />

      <Link className={"link"} to={"/"}>
        <div className={"nav-logo--wrapper"}>
          <img className={"nav-logo"} src={logo} alt="" />
        </div>
      </Link>
    </div>
  );
};

export default Logo;
