import React, {useEffect, useState} from "react";
import {useAuth} from "../../contexts/AuthProvider";
import UserService from "../../services/userService";
import {useHistory} from 'react-router-dom'

const Filled = ({isFilled, id, isWhitelist}) => {
    const history = useHistory()
    const {userToken, refreshMyInfo, currentUser} = useAuth();

    const userService = new UserService();
    const [isFirstOpen, setIsFirstOpen] = useState(true)

    const [filled, setFilledItem] = useState(false)

    const handleChangeFilled = () => {
        userService.setFilledWhitelist(id, userToken, !filled, isWhitelist).then((response) => {
            refreshMyInfo(response.data)
        })
        setFilledItem(prev => !prev);
    }
    const handleIsFilled = () => {
        if(isFirstOpen) {
            return currentUser?.filledWhitelists?.includes(id) || currentUser?.filledAirdrops?.includes(id);
        }
        else return filled
    }

    useEffect(()=> {
        if((currentUser?.filledWhitelists !== undefined ||  currentUser?.filledAirdrops !== undefined) && isFirstOpen){
            setFilledItem(currentUser?.filledWhitelists?.includes(id) || currentUser?.filledAirdrops?.includes(id))
            setIsFirstOpen(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentUser])
    return (
        <div className={"filled-wrapper"}>
                <div className={"filled"}>
                    <div onClick={() => handleChangeFilled()} className={"filled-circle"} >
                        {filled  ? <div className={"filled-icon-wrapper"}>
                            <img className={"filled-icon"} src={require('../../assets/icons/good-icon.svg').default} alt=""/>
                        </div> : ""}
                    </div>
                    {filled ?<div className={"filled-text filled-text--filled"}>FILLED</div> :  <div className={"filled-text filled-text--notfilled"}>NOT FILLED</div> }
                </div>
            <div onClick={() => history.goBack()} className={"close-icon-filled-wrapper"}>
                <img className={"close-icon-filled"} src={require('../../assets/icons/close-icon.svg').default} alt=""/>
            </div>
        </div>

    )
}

export default Filled;