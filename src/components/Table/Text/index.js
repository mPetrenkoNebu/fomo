import React from "react";

const Text = ({text}) => {
    return (
        <div className={"table-text"}>
            {text}
        </div>
    )
}

export default Text;