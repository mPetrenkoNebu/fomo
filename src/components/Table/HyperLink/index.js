import React from "react";
import {  useHistory } from "react-router-dom";
import { useAuth } from "../../../contexts/AuthProvider";

const HyperLink = ({ link }) => {
  const { currentUser } = useAuth();
  const history = useHistory();
  const handleOpenHyperLink = () => {
      if(link.startsWith("http") || link.startsWith("https") || link.startsWith("www")){
            window.open(link, '_blank')
      }else{
          if(currentUser){
              history.push(link)
          }else{
              history.push("/login")
          }
      }
  };
  return (
    <div className={"table-hyperlink"}>
        <div className={"table-hyperlink-link"} onClick={() => handleOpenHyperLink()}>
            <div className={"table-hyperlink-image--wrapper"}>
                <img
                    className={"table-hyperlink-image"}
                    src={require("../../../assets/icons/hyberlink-icon.svg").default}
                    alt=""
                />
            </div>
        </div>

    </div>
  );
};

export default HyperLink;
