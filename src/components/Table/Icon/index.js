import React from "react";

const Icon = ({ icon }) => {
  return (
    <div className={"table-icon--container"}>
      <div className={"table-icon--wrapper"}>
        {icon === "" ? (
          <div className={"table-icon"}/>
        ) : (
          <img className={"table-icon"} src={icon} alt="" />
        )}
      </div>
    </div>
  );
};

export default Icon;
