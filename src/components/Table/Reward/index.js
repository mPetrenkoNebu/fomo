import React from "react";

const Reward = ({reward}) => {
    return (
        <div className={"table-reward"}>
            <div>{reward.quantity}</div>
            <div>({reward.name})</div>
        </div>
    )
}

export default Reward;