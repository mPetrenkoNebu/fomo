import React from "react";

const Filled = ({ isFilled }) => {
  return (
      <div className={"table-filled--wrapper"}>
        <div className={"table-filled"}>
          {isFilled ? (
              <img
                  className={"table-filled-icon"}
                  src={require("../../../assets/icons/good-icon.svg").default}
                  alt=""
              />
          ) : (
              <img
                  className={"table-filled-icon"}
                  src={require("../../../assets/icons/warning.svg").default}
                  alt=""
              />
          )}
        </div>
      </div>

  );
};

export default Filled;
