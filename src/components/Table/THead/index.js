import React from "react";

const THead = ({items}) => {
    return (
        <thead>
            <tr>
                {items.map((item) => (
                    <th className={"table-th"}>{item}</th>
                ))}
            </tr>
        </thead>
    )
}

export default THead;