import React from "react";
import {  useHistory } from "react-router-dom";
import { useAuth } from "../../../contexts/AuthProvider";

const Info = ({ link }) => {
  const { currentUser } = useAuth();
  const history = useHistory();
  const handleOpenInfo = () => {
    if (currentUser) {
      history.push(link);
    } else {
      history.push("/login");
    }
  };
  return (
    <div className={"table-info"}>
      <div onClick={() => handleOpenInfo()}>
        <div className={"table-info-image--wrapper"}>
          <img
            className={"table-info-image"}
            src={require("../../../assets/icons/instruction-icon.svg").default}
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default Info;
