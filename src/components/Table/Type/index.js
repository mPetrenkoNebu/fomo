import React from "react";

const Type = ({ types }) => {
    return (
        <div className={"table-type"}>
            {types?.map((item, index) => (
                <div style={{color: `#${item.color}`}} className={"table-type-text"}>{item.name} {types.length - 1 !== index ? "/"  : ""}</div>
            ))}
        </div>
    )
}

export default Type;