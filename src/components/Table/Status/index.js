import React from "react";
import { TableText } from "../../index";

const Status = ({ isActive }) => {
  return (
    <div className={"table-status--wrapper"}>
      <div className={"table-status"}>
        {isActive ? (
          <img className={"table-status-image"} src={require("../../../assets/icons/green-circle.svg").default} alt="" />
        ) : (
          <img className={"table-status-image"} src={require("../../../assets/icons/red-circle.svg").default} alt="" />
        )}
      </div>
      <TableText text={isActive ? "ACTIVE" : "ENDED"} />
    </div>
  );
};

export default Status;
