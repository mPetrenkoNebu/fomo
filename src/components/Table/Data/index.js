import React from "react";
import {romanMonths} from "../../../data/romanMonths";

const Data = ({ isGood, data }) => {

    const handleAirdropEnd = () => {
        const dataTmp = new Date(data);
        const dataNow = new Date();
        if(dataNow?.valueOf() > data?.valueOf())
            return "0 days"
        let delta = Math.abs(dataTmp - dataNow) / 1000;
        const days = Math.floor(delta / 86400);
        delta -= days * 86400;
        const hours = Math.floor(delta / 3600) % 24;
        return  `${days} days ${hours} hours`
    }

    const getData = () => {
        const dataTmp = new Date(data)
        return `${dataTmp.getDate()}.${romanMonths[dataTmp.getMonth()]}.${dataTmp.getFullYear()}`
    }
  return (
    <div className={"table-data"}>
      <div className={"table-data-text"}>
        {getData()}
        {isGood !== undefined ? (
          <div className={"table-data-img--wrapper"}>
            <img
              className={"table-data-img"}
              src={
                isGood
                  ? require("../../../assets/icons/green-circle.svg").default
                  : require("../../../assets/icons/red-circle.svg").default
              }
              alt=""
            />
          </div>
        ) : (
          ""
        )}
      </div>
      <div className={"table-data--small"}>({handleAirdropEnd()})</div>
    </div>
  );
};

export default Data;
