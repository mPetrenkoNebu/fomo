import React, { useRef, useState } from "react";

const Link = ({ icon, name, link, media }) => {
  const [copiedLink, setCopiedLink] = useState("");

  const handleCopy = () => {
    navigator.clipboard.writeText(link);
    setCopiedLink(link);
    setTimeout(() => {
      setCopiedLink("");
    }, 1000);
  };
  const linkRef = useRef(null);
  return (
    <div className={"instruction-link"}>
      <div className={"instruction-icon-wrapper"}>
        <img className={"instruction-icon"} src={icon} alt="" />
      </div>
      <div>
        <div className={"instruction-name"}>
          {media !== undefined ? `Follow ${name} ${media}` : name}
        </div>
        <div className={"instruction-link-wrapper"}>
          <div ref={linkRef} className={"instruction-link-text"}>
            {link}
          </div>
          <div onClick={() => handleCopy()} className={"instruction-copy"}>
            <img
              className={"instruction-icon"}
              src={require("../../../assets/icons/copy-icon.svg").default}
              alt=""
            />
          </div>
          {copiedLink === link && link !== "" ? (
            <div className={"instruction-link-copy"}>Copied!</div>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default Link;
