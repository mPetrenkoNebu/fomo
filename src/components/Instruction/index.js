import React from "react";
import Link from "./Link";
import {Profile} from "../index";

const Instruction = ({instructions}) => {
    const getMedia =(media) => {
        switch (media.name){
            case "Retweet":
                return  <Link key={media.id} name={"Kylin"} icon={require('../../assets/icons/twitter-ref-icon.svg').default} link={media?.value} media={"Twitt"}/>
            case "Telegram":
                return <Link  key={media.id} name={"Kylin"} icon={require('../../assets/icons/telegram-icon.svg').default} link={media?.value} media={"Telegram"}/>
            case "Twitter":
                return  <Link  key={media.id} name={"Kylin"} icon={require('../../assets/icons/twitter-icon.svg').default} link={media?.value} media={"Twitter"}/>
            case "file":
                return   <Link  key={media.id} name={"Fill the form"} icon={require('../../assets/icons/file-icon.svg').default}  link={media?.value}/>
            default:  return   <Link  key={media.id} name={media.name} icon={require('../../assets/icons/file-icon.svg').default}  link={media?.value}/>

        }
    }

    return (
        <div >
            <div className={"instruction-title"}>
                Instruction:
            </div>
            <div className={"instruction-wrapper"}>
                <div>
                    {instructions?.map((item) => (
                        getMedia(item)
                    ))}
                </div>
                <div>
                    <Profile />
                </div>
            </div>
        </div>
    )
}

export default Instruction;