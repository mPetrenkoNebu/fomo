import React from "react";
import { Link } from "react-router-dom";

const Breadcrumbs = ({ pathItems }) => {
  const pathLength = pathItems.length - 1;
  return (
    <div className={"breadcrumbs"}>
      {pathItems.map((item, index) =>
        pathLength !== index ? (
          <Link key={item.name} className={"link"} to={item.link}>
            <div className={"breadcrumbs-item breadcrumbs-arrow"}>
              <div>{item.name}</div>
              <div className={"breadcrumbs-icon--wrapper"}>
                <img
                  src={
                    require("../../assets/icons/arrow-right-icon.svg").default
                  }
                  className={"breadcrumbs-icon"}
                  alt=""
                />
              </div>
            </div>
          </Link>
        ) : (
          <div key={item.name} className={"breadcrumbs-item"}>{item.name}</div>
        )
      )}
    </div>
  );
};

export default Breadcrumbs;
