import React from "react";
import { Link } from "react-router-dom";
import {romanMonths} from "../../data/romanMonths";

const NewsItem = ({ title, text, imageUrl, data, id }) => {
  const getDate = () => {
    const tmpDate = new Date(data);
    return `${tmpDate.getDate()}.${romanMonths[tmpDate.getMonth()]}.${tmpDate.getFullYear()}`
  }
  return (
    <div className={"news-container"}>
      <Link className={"link"} to={`/news/${id}`}>
        <div className={"news--wrapper"}>
          <div className={"news-main"}>
            <div className={"news-image--wrapper"}>
              <img className={"news-image"} src={imageUrl} alt="" />
            </div>
            <div className={"news-info"}>
              <div className={"news-title"}>{title}</div>
              <div className={"news-text"}>{text?.split("*!brk!*").join(' ')}</div>
            </div>
          </div>
          <div className={"news-data"}>{getDate()}</div>
        </div>
        <div className={"news-separator"} />
      </Link>
    </div>
  );
};

export default NewsItem;
