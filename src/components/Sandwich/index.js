import React from "react";

const Sandwich = ({setIsNavOpen, isNavOpen}) => {
    return (
        <div onClick={() => setIsNavOpen(prev => !prev)} className={`sandwich ${isNavOpen ? 'is-active' : ""}`}>
            <div className="sandwich__line sandwich__line--top"/>
            <div className="sandwich__line sandwich__line--middle"/>
            <div className="sandwich__line sandwich__line--bottom"/>
        </div>
    )
}

export default Sandwich;