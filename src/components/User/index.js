import React from "react";
import userIcon from "../../assets/icons/user.svg";
import { Link } from "react-router-dom";
import { Logout } from "../index";
import { useAuth } from "../../contexts/AuthProvider";

const User = () => {
  const { currentUser } = useAuth();
  return (
    <div className={"nav-user--wrapper"}>
      {!currentUser ? (
        <Link className={"link"} to={"/login"}>
          <div className={"nav-button"}>Connect</div>
        </Link>
      ) : (
        ""
      )}
      {currentUser ? (
        <div className={"nav-user"}>
          <img className={"nav-image"} src={userIcon} alt="" />
        </div>
      ) : (
        ""
      )}

      {currentUser ? <Logout /> : ""}
    </div>
  );
};

export default User;
