import React from "react";

const ActionButton = ({name, onClick, isDisable}) => {
    return (
        <button onClick={onClick} disabled={isDisable} className={"action-button"}>
            {name}
        </button>
    )
}

export default ActionButton;