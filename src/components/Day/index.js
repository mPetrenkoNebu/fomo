import React from "react";
import {Link} from "react-router-dom";
import {useAuth} from "../../contexts/AuthProvider";

const Day = ({day, items, selectedDay, selectedCategory}) => {
    const { currentUser } = useAuth();

    const getColorOfItem = (item) => {
        switch (item.type) {
            case "WHITELIST":
                return "#00FFD8"
            case "AIRDROP":
                return "#FFEB00"
            case "EVENT":
                return "#9C00D6";
            default: return "";
        }
    }
    const getFilterResult = (dataEnds, data) => {
        return new Date(dataEnds).getDate() === data.getDate() && new Date(dataEnds).getMonth() === data.getMonth() && new Date(dataEnds).getFullYear() === data.getFullYear()
    }
    const getDayName = (dayTime) => {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];
        return `${monthNames[dayTime.getMonth()]} ${dayTime.getDate()}`;
    }
    return (
        <div className={`day-wrapper ${getFilterResult(selectedDay, day) ? "day-wrapper--active" : ""}`}>
            <div className={"day-title"}>{getDayName(day)}</div>
            <div>
                {items?.filter((item) => selectedCategory === "" || selectedCategory.includes(item.type) ).map((item) => (
                    <Link className={"link day-warning--wrapper "} to={`/${item.type === "WHITELIST" ? "whitelists" : "airdrops"}/${item.id}`} >
                        <div style={{width: '100%', position: 'relative'}}>
                            <div key={item.id} style={{color: getColorOfItem(item), border: `1px solid ${getColorOfItem(item)}`}} className={"day-item"}>{item.name ?? item.project}</div>
                            <div style={{display: currentUser?.filledAirdrops?.includes(item.id) ||  currentUser?.filledWhitelists?.includes(item.id) ? "none" : "block"}} className={"day-warning"}>
                                <img  className={"calendar-warning-icon calendar-warning-icon--small"} src={require('../../assets/icons/warning.svg').default} alt=""/>
                            </div>
                        </div>
                    </Link>
                ))}
            </div>
        </div>
    )
}

export default Day  ;