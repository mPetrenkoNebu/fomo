import React from "react";
import { romanMonths } from "../../data/romanMonths";
import {Link} from "react-router-dom";

const EditElement = ({ name, data, link }) => {
  const getDate = () => {
    const tmpDate = new Date(data);
    return `${tmpDate.getDate()}.${
      romanMonths[tmpDate.getMonth()]
    }.${tmpDate.getFullYear()}`;
  };

  return (
      <Link className={"link"} to={`/admin/${link}`}>
          <div className={"edit-element"}>
              <div>{name}</div>
              <div>{getDate()}</div>
          </div>
      </Link>

  );
};

export default EditElement;
