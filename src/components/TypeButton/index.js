import React from "react";

const TypeButton = ({ type, onClick, selectedType }) => {
  return (
    <div
      onClick={() => onClick(type)}
      style={{
        color: `${
          selectedType?.name === type.name || selectedType?.name === undefined
            ? `#${type.color}`
            : "#555656"
        }`,
        border: `1px solid ${
          selectedType?.name === type.name || selectedType?.name === undefined
            ? `#${type.color}`
            : "#555656"
        }`,
      }}
      className={`type-button`}
    >
      {type?.name}
    </div>
  );
};

export default TypeButton;
